// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //googleKey: 'AIzaSyCwYzvuQYBzOgRvrRgKrYcy9WxoNglvM10'
  googleKey: 'AIzaSyDbzIFSdb9vxw_4AQv0fkAKhkS4DXWLgF0',
  firebaseConfig: {
    apiKey: "AIzaSyBIOtR5o0ERSR00qoDmcIow0Z8AvYivtlk",
    authDomain: "confflux-f6ed0.firebaseapp.com",
    databaseURL: "https://confflux-f6ed0.firebaseio.com",
    projectId: "confflux-f6ed0",
    storageBucket: "confflux-f6ed0.appspot.com",
    messagingSenderId: "930793394930",
    appId: "1:930793394930:web:3a147557e9858b3ded366b",
    measurementId: "G-N9164VMBG0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from '../app/services/language.service';
import { Subscription } from 'rxjs';
import { BusinessLogicService } from '../app/services/businessLogic.service';
import { getParams } from '../app/models/types';
import { ParamsService } from 'src/app/services/params.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  searchValue: string;
  locationSub: Subscription;
  enableSearch = false;
  showError;
  isSpinningCategories: boolean = false;
  categories = [];
  features = [
    {
      image: 'lupa.svg',
      title: {
        en: 'Advanced search',
        es: 'Búsqueda Avanzada',
        fr: 'Recherche avancée',
      },
      desc: {
        en: 'Find what you need close to you.',
        es: 'Encuentra lo que necesitas cerca de ti.',
        fr: 'Trouvez des établissement proches de vous.',
      },
    },
    {
      image: 'filas.svg',
      title: { en: 'Without queue', es: 'Sin Filas', fr: 'Plus de files d’attende' },
      desc: {
        en: 'Book a slot and avoid queues',
        es: 'Reserva tu turno y evita congestiones.',
        fr: 'Réalisez des réservations et évitez des files d\'attente.',
      },
    },
    {
      image: 'espacio.svg',
      title: {
        en: 'Smart Spaces',
        es: 'Espacios Inteligentes',
        fr: 'Espaces intelligents',
      },
      desc: {
        en: 'Visitor count, heat maps, and video-analytics for Establishments.',
        es:
          'Conteo de visitantes, Mapas de calor, y video-analítica para Establecimientos.',
        fr:
          'Nombre de visiteurs, cartes thermiques et analyses vidéo pour les établissements.',
      },
    },
    {
      image: 'entregas.svg',
      title: { en: 'Deliveriy', es: 'Entregas', fr: 'Livraison' },
      desc: {
        en: 'Pick up your product at a point without waiting.',
        es: 'Recoge tu producto en un punto sin esperas.',
        fr: 'Choisissez votre produit sans temps d\'attente.',
      },
    },
  ];
  allies = [
    { image: 'oracle.png' },
    { image: 'UAndes.png' },
    { image: 'innpulsa.png' },
    { image: 'sorbone.png' },
    { image: 'franceMovilities.png' },
  ];

  //Texts
  TiendaEjText = { en: 'Store', es: 'Establecimiento', fr: 'Établissement' };
  locationText = {
    en: 'Please enable your GPS',
    es: 'Por favor active su GPS',
    fr: 'Veuillez activer votre GPS',
  };
  searchButtonText = { en: 'Search', es: 'Buscar', fr: 'Chercher' };
  seeCategoriesText = {
    en: 'See categories',
    es: 'Ver categorías',
    fr: 'Voir les catégories',
  };
  IamOwnerText = {
    en: "I'm owner",
    es: 'Soy propietario',
    fr: 'Je suis propriétaire',
  };
  IamVisitorText = {
    en: "I'm visitor",
    es: 'Soy visitante',
    fr: 'Je suis visiteur',
  };
  ScanStoreText = {
    en: 'Scan to enter',
    es: 'Escanear para entrar',
    fr: 'Scannez pour entrer',
  };
  introText = {
    en:
      'Confflux allows controlling the level of inflows through an efficient shift system. Search here your favorite establhisment and found out our new services.',
    es:
      'Confflux permite controlar el nivel de afluencia en establecimientos y brinda la posibilidad de visitas sin filas. Busca tu establecimiento favorito y descubre los nuevos servicios que tiene para ti.',
    fr:
      "Confflux permet de contrôler les affluences grâce à un système de réservations.",
  };
  howWorks = {
    en: 'How does it work?',
    es: '¿Cómo funciona?',
    fr: 'Comment ça fonctionne?',
  };
  howWorks2 = {
    en: 'CONFFLUX "Without Queue"',
    es: 'CONFFLUX "Sin Filas"',
    fr: 'CONFFLUX "Sans files d’attente"',
  };
  registerStoreText = {
    en: 'Register your establishment',
    es: 'Inscribe tu establecimiento',
    fr: 'Créer un établissement',
  };
  registerStoreTextp1 = {
    en:
      'For establishments, Confflux gives the possibility to gain visibility with our Internet options, and offering their visitors an experience without queues and waiting time. You can register your stablishments for free and enjoy the Confflux features from the day one.',
    es:
      'Puedes inscribir tu establecimiento a CONFFLUX sin costo, y acceder a las distintas funcionalidades desde el primer momento. CONFFLUX busca ganar visibilidad para sus establecimientos, mediante su oferta en Internet, y así mismo ofrecer a sus visitantes una experiencia sin filas ni congestión.',
    fr:
      "Vous pouvez vous inscrire gratuitement à CONFFLUX et accéder aux différentes fonctionnalités dès le début. CONFFLUX permet aux établissements de gagner en visibilité et en fiabilité vis-à-vis leurs clients, et aussi d’offrir à ses visiteurs une expérience sans files d’attente ni saturation.",
  };
  registerStoreTextp2 = {
    en:
      'Every day we are making new solutions for our establishments with the help of new high technologies such and Artificial Intelligence, always looking at the way to enhance and make secure the experience of clients and visitors. ',
    es:
      'Seguimos avanzando día a día con diferentes funciones innovadoras mediante Inteligencia Artificial y altas tecnologías para nuestros establecimientos y así ellos, puedan ofrecer una experiencia segura para sus clientes y visitantes.',
    fr:
      "Nous progressons régulièrement avec différentes fonctionnalités innovantes grâce à l’intelligence artificielle et aux technologies de pointe qui bénéficient nos établissements. ",
  };
  OwnerManageText = {
    en: 'You can manage your store here',
    es: 'Puedes administrar tu establecimiento aquí',
    fr: 'Gérez votre établissement ici',
  };
  moreFeaturesText = {
    en: 'More CONFFLUX Features',
    es: 'Más Caracteristicas de CONFFLUX',
    fr: 'Ce que  CONFFLUX  vous offre',
  };
  aboutConfText = {
    en:
      'We are a startup born of a young group of engineers and scientists from Europa and Latin America, with the objective in mind to give innovative tools to potentiate and give new channels to different establishments for their clients and visitors, even knowing the world’s new situation, the restriction for space, recession, and the health emergency we are facing.',
    es:
      'Somos una startup compuesta por jóvenes ingenieros y científicos de Europa y Latinoamérica, con el objetivo de brindar herramientas innovadoras que busquen potenciar a diferentes establecimientos, ayudandolos a ofrecer nuevos canales para llegar a sus clientes y visitantes, teniendo en cuenta las restricciones de espacio, la recesión ecómica y la crisis de salubridad de la actualidad.',
    fr:
      "Nous sommes une startup composée d'un jeune groupe d'ingénieurs entrepreneurs d'Europe et d'Amérique latine, ayant pour objectif d’offrir des solutions innovantes pour renforcer la visibilité et la fiabilité des différents établissements vis-à-vis leurs clients et visiteurs.",
  };
  AllyTitleText = { en: 'Allies', es: 'Aliados', fr: 'Nos associés' };
  AllyTitleTextp1 = {
    en:
      'We joined the initiative of the National Government, INNpulsa Colombia and Fedesoft to contribute free of charge:',
    es:
      'Nos unimos a la iniciativa de Gobierno Nacional, INNpulsa Colombia y Fedesoft para aportar de forma gratuita:',
    fr:
      "Nous avons rejoint l'initiative du gouvernement national, INNpulsa Colombia et Fedesoft pour contribuer gratuitement:",
  };
  AllyTitleTextp2 = {
    en: 'Application access and use of ',
    es: 'Acceso y uso de aplicación ',
    fr: "Accès et utilisation de l'application ",
  };
  AllyTitleTextp3 = {
    en: 'standard version, unlimited time ',
    es: 'versión estándard, tiempo ilimitado ',
    fr: 'version standard, durée illimitée ',
  };
  AllyTitleTextp4 = {
    en: 'time to join because ',
    es: 'momento de unirnos porque ',
    fr: 'le temps de rejoindre parce',
  };
  AllyTitleTextp5 = {
    en: "We're all part of the solution!",
    es: '¡Todos somos parte de la solución!',
    fr: 'Nous faisons tous partie de la solution!',
  };

  buttons = [
    { en: 'OWNER REGISTRATION', es: 'REGISTRAR ESTABLECIMIENTO', fr: 'INSCRIPTION ÉTABLISSEMENT' },
    { en: 'FEATURES', es: 'CARACTERISTICAS', fr: 'TRAITS' },
    { en: 'CONTACT US', es: 'CONTÁCTENOS', fr: 'CONTACTER' },
  ];
  h1Headers = [
    {
      en: 'Search, book and visit without queue!',
      es: '¡Busca, reserva y visita sin filas!',
      fr: 'Réservez et évitez les files d\'attente!',
    },
    {
      en: 'About The Application',
      es: 'Acerca de la aplicación',
      fr: 'A propos de l’application',
    },
    {
      en: 'Application Features',
      es: 'Características de la aplicación ',
      fr: 'Caractéristiques de l’application',
    },
    { en: 'About CONFFLUX', es: 'Sobre CONFFLUX', fr: 'À propos de CONFFLUX' },
    { en: 'Contact Us', es: 'Contáctenos', fr: 'Nous contacter' },
  ];
  buttonsForm = [
    { en: 'Try it now', es: 'Accede al App aquí:', fr: 'Essayez-le' },
  ];
  buttonsFormLinks = [
    {
      en:
        'https://docs.google.com/forms/d/e/1FAIpQLSffAipIoQJEhalNKA_2mqglSG-5zVr9I70X6kahcRqNc4S01g/viewform',
      es:
        'https://docs.google.com/forms/d/e/1FAIpQLScPEI1sRolgomgsiklxj7AN5B1S_Nr6L1SrWBaJtC45mf1mqA/viewform',
      fr:
        'https://docs.google.com/forms/d/e/1FAIpQLSdPuTuVjkYbbrVcozB4Xuo9GDlKxplCAtgJSpwWMUO8H1UrIQ/viewform',
    },
  ];
  h2Headers = [
    {
      en: 'Creating safe spaces',
      es: 'Creando espacios seguros ',
      fr: 'Créer des espaces sûrs',
    },
    {
      en: 'Problems to be solved and how we do it',
      es: '¿Cuáles son los problemas y qué soluciones proponemos?',
      fr: 'Quel sont les problèmes à résoudre et comment y parvernir?',
    },
    { en: 'Needs', es: 'Problemáticas', fr: 'Problématiques' },
    {
      en: 'Confflux provides',
      es: 'Confflux te brinda',
      fr: 'Confflux vous offre',
    },
    {
      en: 'The People Behind The Application',
      es: '¿Quiénes somos?',
      fr: 'Qui sommes nous?',
    },
  ];
  texts = [
    {
      en:
        'Visitors can find less crowded places and schedule shifts. Owners can register Establishments. ',
      es:
        ' Los Visitantes pueden encontrar lugares menos concurridos y agendar turnos. Los Propietarios pueden registrar Establecimientos.',
      fr:
        'Confflux offre des informations sur le niveau d’affluences dans n’importe quel établissement.',
    },
    {
      en: [
        `- People need to access services everyday without having to wait endless queues`,
        `- Establishments (supermarkets, offices, shops, etc.) need to provide to their customers with confidence by proposing spaces free of crowds`,
        `- The government needs to revive the local economy by creating safe open spaces for people`,
      ],
      es: [
        `- Las personas necesitan acceder a los servicios de la vida cotidiana sin tener que hacer  filas interminables`,
        `- Los establecimientos  (supermecados, oficinas, tiendas, etc.)  necesitan brindar confianza a sus clientes proponiendo espacios  libres de cogestión`,
        `- El gobierno necesita reactivar la economia local creando espacios seguros para las personas`,
      ],
      fr: [
        `- Les gens ont besoin d’accéder aux services de la vie quotidienne sans avoir à faire des files d’attentes interminables.`,
        `- Les établissements  (supermarchés, bureaux, magasins, etc.)   doivent assurer leurs clients en leur proposant des espaces libres de cogestion.`,
        `- Le gouvernement doit relancer l’économie locale en créant des espaces sûrs pour les gens.`,
      ],
    },
    {
      en: [
        `- Real time Information on the level of occupancy in establishments`,
        `- Reserve and online scheduling of shifts to avoid queues at establishments`,
        `- A customized profile setting to allow establishments to publish relevant information to customers`,
        `- Crowds information in open spaces such as bus stations or shopping centers through camera image analysis using AI`,
      ],
      es: [
        `- Información sobre el nivel ocupación en los establecimientos en tiempo real`,
        `- Reserva y agendamiento de turnos online  con el fin de evitar filas de espera en establecimientos`,
        `- Un perfil personalizado del establecimiento que le permite publicar información relevante para clientes`,
        `- Información de la congestión en espacios abiertos como estaciones de buses o centros comerciales a traves del análisis de imagen de camaras usando la inteligencia artificial `,
      ],
      fr: [
        `- Des Informations sur le niveau d’affluences dans les établissements en temps réel.`,
        `- Des réservations et possibilité de prise de rendez-vous en ligne afin d’éviter les files d’attente dans les établissements.`,
        `- Un profil personnalisé de l’établissement qui vous permet de publier des informations pertinentes adressées aux clients.`,
        `- Des informations sur le niveau de congestion dans les espaces ouverts tels que les gares, les stations de bus ou les centres commerciaux à travers l’analyse d’image par intelligence artificielle.`,
      ],
    },
    {
      en: [
        `We are a group of young professionals from Europe and Latin America, enthusiasts for technology and sustainability.`,
        `Headquarters`,
        `París (Francia), Bogotá (Colombia)`,
        `Skills`,
        `Machine Learning-AI, Optimization, Cloud apps developement`,
      ],
      es: [
        `Somos un grupo de jóvenes profesionales de Europa y Latinoamérica, entusiastas por la tecnología y la sostenibilidad.`,
        `Sedes:`,
        `París (Francia), Bogota (Colombia)`,
        `Competencias`,
        `Inteligencia Artificial, Optimizacion, Aplicaciones en la nube`,
      ],
      fr: [
        `Nous sommes une équipe de 12 ingénieurs d’Europe et Amérique du sud passionnés par la technologie et le développement durable.`,
        `Siège social`,
        `Paris 75001`,
        `Compétences`,
        `Traitement d’image avec l’intelligence artificielle, Optimisation du transport, Web development solutions, Cloud computing`,
      ],
    },
  ];

  constructor(
    private paramsService: ParamsService,
    public languageService: LanguageService,
    private router: Router,
    private blService: BusinessLogicService
  ) {
    this.locationSub = this.blService.local_location.subscribe((res: any) => {
      if (res) {
        this.enableSearch = true;
      }
    });
    this.blService.setCurrentLocation();
  }

  ngOnInit(): void {
    this.isSpinningCategories = true;
    this.paramsService
      .getShopsParams()
      .pipe(
        finalize(() => {
          this.isSpinningCategories = false;
        })
      )
      .subscribe((response: getParams) => {
        let params = response.params;
        this.categories = params.filter(function (item) {
          return item.name === 'Categories';
        })[0].value;
      });
  }

  ownerSection() {
    //document.getElementById('IdOwnerSection').scrollIntoView();
    this.router.navigate(['/owner']);
  }
  feturesSection() {
    document.getElementById('IdFeaturesSection').scrollIntoView();
  }
  ContactUs() {
    document.getElementById('IdContactUs').scrollIntoView();
  }

  searchPlace() {
    if (
      this.searchValue?.length <= 0 ||
      this.searchValue?.trim() === '' ||
      !this.searchValue
    )
      return;
    if (!this.enableSearch) {
      // this.showError = {
      //   show: true,
      //   message: this.locationText[this.languageService.language]
      // }
      this.blService.createMessage('error', this.locationText);
      return;
    }
    this.router.navigate(['user/user-home'], {
      queryParams: { search: this.searchValue },
    });
  }

  searchCategory(title: string) {
    if (!this.enableSearch) {
      // this.showError = {
      //   show: true,
      //   message: this.locationText[this.languageService.language]
      // }
      this.blService.createMessage('error', this.locationText);
      return;
    }
    this.router.navigate(['user/user-home'], {
      queryParams: { category: title },
    });
  }

  goToOwner() {
    this.router.navigate(['/owner']);
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { BusinessLogicService } from '../services/businessLogic.service';
import { ErrorService } from './error.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    // Error handling is important and needs to be loaded first.
    // Because of this we should manually inject the services with Injector.
    constructor(private injector: Injector) { }

    handleError(error: Error | HttpErrorResponse) {

        const errorService = this.injector.get(ErrorService);
        // IN THE FUTURE WE SHOULD HAVE A LOGGING SERVICE const logger = this.injector.get(LoggingService);
        const notifier = this.injector.get(BusinessLogicService);

        let message;
        let stackTrace;

        if (error instanceof HttpErrorResponse) {
            const errorType = errorService.getServerType(error)
            switch (error.error.type) {
                case "MISSING_ARG":
                case "NO_ACCESS":
                    message = errorService.getServerMessage(error);
                    break;
                case "NOT_FOUND":
                    // Maybe this we should handle in the component because can be something specific.
                    message = errorService.getServerMessage(error);
                    break;
                case "WRONG_CREDENTIALS":
                    message = 'The email or the password are wrong';
                    break;
                case "MONGOOSE_ERROR":
                    message = 'We have received an unexpected error. Please try again';
                    break;
                default:
                    message = errorService.getServerMessage(error);
                    break;
            }
            //notifier.createMessage('error', message);
        } else {
            // Client Error happened
            message = errorService.getClientMessage(error);
            stackTrace = errorService.getClientStack(error);
            //notifier.createMessage('error', message);
        }
        // Log the error anyway
        console.error(error);
    }
}
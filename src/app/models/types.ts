export class location {
  latitude: string;
  longitude: string;
  city?: string;
  stateCode?: string;
  postalCode?: string;
  countryCode?: string;
  street?: string;
  type?: string;
  display_name?: string;
  country?: string;
  address?: string;
}

export class point {
  dest_category: string; //category of destination
  location: location; //initial location of person
  status: string; //status of the journey
}

export class historical {
  value: string; //value of the range of time
  dest_category: string; //category of destination
  amount_journeys: number; //amount of journeys tomards this category of destination
}

export class shop {
  _id: string;
  people_inside: number; //actual quantity of people already in the shop
  location: location; //location of the shop
  name: string; //name of the shop
  category: string; //category of the shop
  endHour: string;
  daysClosed: string;
}

export class shops {
  total: number; //number of total shops
  values: shop[]; //shops
}

export class owner {
  email: string;
  password: string;
}

export class sue {
  _id: string;
  id_person: string;
  sue_category: string;
  title: string;
  description: string;
  date: string;
  location: location;
}

export class sues {
  total: number; //number of total sues
  values: sue[]; //sues
}

export class Params {
  name: string;
  value: string[];
}

export class getParams {
  params: Params[];
}

export class googleAutoComplete {
  predictions: {
    description: string;
    id: string;
    matched_substrings: {
      place_id: string;
      reference: string;
    }[];
    structured_formatting: {
      main_text: string;
      secondary_text: string;
    }[];
    types: string[];
  }[];
}

export class OwnerUser {
  constructor(
    public id_owner: string,
    private _token: string,
    private _tokenExpirationDate: Date
  ) {}

  get token() {
    if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate) {
      return null;
    }
    return this._token;
  }
}

export class ManagerUser {
  constructor(
    public id_manager: string,
    private _token: string,
    private _tokenExpirationDate: Date
  ) {}

  get token() {
    if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate) {
      return null;
    }
    return this._token;
  }
}

export class store {
  constructor(
    public location: location,
    public category: { name: string },
    public name: string
  ) {}
}

export class OwnerData {
  constructor(
    public establishments: {
      id: string;
      name: string;
      qr_code: string;
    }[]
  ) {}

  get names() {
    let retVal: string[] = [];
    for (let i = 0; i < this.establishments.length; i++) {
      retVal.push(this.establishments[i].name);
    }
    return retVal;
  }
}

export class place {
  name: string;
  code: string;
}

export class OpeningHours {
  day: number[];
  open_hour: number;
  close_hour: number;
}

export class Establishment {
  id?: string;
  name?: string;
  phone?: number;
  description?: string;
  isActive?: string;
  current_affluences?: number;
  max_affluences_allowed?: number;
  shift_attention_mins?: number;
  shift_schedule_max_hours?: number;
  checkin_max_min?: number;
  max_shifts?: number;
  location?: location;
  opening_hours?: OpeningHours;
  category_name?: string;
  slot_size?: string;
  establishment_pics_url?: string[];
  enableShifting?: boolean;
}
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from '../../services/user.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize, debounceTime } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { location } from '../../models/types';
import { MatDialog } from '@angular/material/dialog';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';
import { LanguageService } from '../../services/language.service';

const elementsPerPage = 10;

@Component({
  selector: 'app-user-turns',
  templateUrl: './user-turns.component.html',
  styleUrls: ['./user-turns.component.css'],
})
export class UserTurnsComponent implements OnInit, OnDestroy {
  total: number = 0;
  pages: number[] = [];
  offset = 0;
  currentPage = 0;
  manualPage = 1;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  validateForm: FormGroup;
  showEditDiv: boolean = false;
  shiftId: string;
  storeId: string;
  shifts: any[];
  isSpinning: boolean = false;
  isSpinningStores: boolean = false;
  pageSize = elementsPerPage;
  searchInput: Subject<string> = new Subject<string>();
  searchText: string;
  displayedColumns: string[] = ['place', 'hour', 'date', 'manual', 'edit'];
  dataSet;
  dataSetGlobal;
  dataSource;
  minDate: string = '2013-12-25';
  maxDate: string = '2113-12-25';
  locationSub: Subscription;
  location: location;
  store: any;
  showAll: boolean = false;

  day_name = [
    {en: 'Sunday', es: 'Domingo', fr: 'Dimanche'},
    {en: 'Monday', es: 'Lunes', fr: 'Lundi'},
    {en: 'Tuesday', es: 'Martes', fr: 'Mardi'},
    {en: 'Wednsday', es: 'Miércoles', fr: 'Mercredi'},
    {en: 'Thursday', es: 'Jueves', fr: 'Jeudi'},
    {en: 'Friday', es: 'Viernes', fr: 'Vendredi'},
    {en: 'Saturday', es: 'Sábado', fr: 'Samedi'},
  ];

  //Texts
  MyTurns = {en: 'My shifts', es: 'Mis turnos', fr: 'Mes shifts'};
  SearchText = {en: 'Search one shift', es: 'Busque un turno', fr: 'Cherchez une shift'};
  ManualText = {en: 'MANUAL', es: 'MANUAL', fr: 'MANUELLE'};
  PlaceText = {en: 'PLACE', es: 'LUGAR', fr: 'PLACE'};
  DateText = {en: 'DATE', es: 'FECHA', fr: 'DATE'};
  HourText = {en: 'HOUR', es: 'HORA', fr: 'HEURE'};
  MenuText = {en: 'Menu', es: 'Menú', fr: 'Menu'};
  CodeText = {en: 'CODE', es: 'CÓDIGO', fr: 'CODE'};
  ShiftText = {en: 'SHIFT', es: 'TURNO', fr: 'SHIFT'};
  ObservationsText = {en: 'OBSERVATIONS', es: 'OBSERVACIONES', fr: 'OBSERVATIONS'};
  CancelText = {en: 'CANCEL', es: 'CANCELAR', fr: 'ANNULER'};
  ConfirmText = {en: 'CONFIRM', es: 'CONFIRMAR', fr: 'CONFIRMER'};
  EditText = {en: 'Edit', es: 'Editar', fr: 'Éditer'};
  DeleteText = {en: 'Delete', es: 'Eliminar', fr: 'Supprimer'};
  EditBoxText = {en: 'EDIT YOUR SHIFT', es: 'EDITE SU TURNO', fr: 'ÉDITEZ SON SHIFT'};
  AreSureText = {en: 'Are you sure you want to delete this shift?', es: '¿Está seguro de borrar este turno?', fr: 'Voulez-vous vraiment supprimer ce shift?'};
  TitleSureText = {en: 'Confirm', es: 'Confirmar', fr: 'Confirmer'};
  NoSureText = {en: 'No', es: 'No', fr: 'Non'};
  YesSureText = {en: 'Yes', es: 'Sí', fr: 'Oui'};
  DatePreviousText = {en: 'This date is previous to actual date', es: 'El día es anterior a hoy', fr: 'Cette date est antérieure à la date réelle'};
  DateFutureErrText = {en: 'Date must be less than 2 days in the future', es: 'La fecha seleccionada no debe superar dos días en el futuro', fr: 'La date doit être inférieure à 2 jours dans le futur'};
  NoShiftsErrText = {en: 'On this date this establishment is closed', es: 'En esta fecha este establecimiento está cerrado', fr: 'A cette date cet établissement est fermé'};
  OpenText = {en: 'Open', es: 'Abierto', fr: 'Ouvert'};
  AllDaysText = {en: 'All days', es: 'Todos los días', fr: 'Tous les jours'};
  showAllText = {en: 'Show all shifts', es: 'Mostrar todos los turnos', fr: 'Afficher tous les shifts'};
  showLessText = {en: 'Show just next shifts', es: 'Mostrar solo los turnos siguientes', fr: 'Afficher uniquement les shifts suivants'};

  constructor(
    private userService: UserService,
    private blService: BusinessLogicService,
    public dialog: MatDialog,
    private router: Router,
    private fb: FormBuilder,
    public languageService: LanguageService
  ) {
    this.validateForm = this.fb.group({
      dateTurn: [null, [Validators.required]],
      shiftTurn: [null, [Validators.required]],
      observationsTurn: [''],
    });
  }

  ngOnInit(): void {
    this.isSpinning = true;
    this.checkShiftsUser();
    this.initKeyUpListenerAddress();
    this.minDate = this.getDay(null);
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          this.location = res;
        }
      }
    );
    this.blService.setCurrentLocation();
  }

  checkShiftsUser() {
    this.userService
      .getShiftsUser()
      .pipe(
        finalize(() => {
          this.isSpinning = false;
        })
      )
      .subscribe(
        (response: any) => {
          if (response.success) {
            this.dataSetGlobal = response.shifts;
            this.dataSetGlobal.forEach((item) => {
              let date = new Date(item.shift_date).getTime() + (item.establishment.slot_size + 15) * 60 * 1000;
              let today = new Date().getTime();
              item['shift_passed'] = date < today ? true : false;
            })
            this.dataSet = this.dataSetGlobal;
            this.total = this.dataSet.length;
            this.dataSource = this.dataSet.slice(0, elementsPerPage);
          } else {
            let message = {
              en: 'There was an error loading shifts',
              es: 'Hubo un error al cargar los turnos',
              fr: 'Une erreur s\'est produite lors du chargement des shifts'
            };
            this.blService.createMessage('error', message);
          }
        },
        (err) => {
          let message = {
            en: 'There was an error loading shifts',
            es: 'Hubo un error al cargar los turnos',
            fr: 'Une erreur s\'est produite lors du chargement des shifts'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  initKeyUpListenerAddress() {
    this.searchInput.pipe(debounceTime(700)).subscribe((data: string) => {
      this.dataSet = this.dataSetGlobal;
      if (this.searchText.trim() === '') {
        this.total = this.dataSet.length;
        this.resetPaginator();
        return;
      }
      let searchText = this.searchText.toLowerCase();
      this.dataSet = this.dataSet.filter(function (item) {
        return item.establishment.name.toLowerCase().includes(searchText);
      });
      this.total = this.dataSet.length;
      this.resetPaginator();
    });
  }

  changed() {
    this.searchInput.next();
  }

  checkHour(hours) {
    if (!hours) return '';
    let hour = new Date(hours).toLocaleString('en-US').split(' ')
    return hour[1].split(':').slice(0,2).join(':') + ' ' + hour[2];
  }

  checkDate(date) {
    return new Date(date).toLocaleString().split(' ')[0];
  }

  editTurn(id) {
    this.shiftId = id;
    let establishment = this.dataSetGlobal.filter(function (item) {
      return item._id === id;
    })[0]?.establishment;
    if (!establishment) {
      let message = {
        en: 'There was a problem with the store',
        es: 'Hubo un problema con el establecimiento',
        fr: 'Il y avait un problème avec l\'établissement'
      };
      this.blService.createMessage('error', message);
      return;
    }
    this.store = establishment;
    this.storeId = establishment._id;
    let shift_schedule_max_hours = establishment.shift_schedule_max_hours;
    let max_date = new Date().getTime() + (48*60*60*1000);
    this.maxDate = this.getDay(max_date);
    this.userService
      .getShift(id)
      .pipe(
        finalize(() => {
          //TODO
        })
      )
      .subscribe(
        (response: any) => {
          if (response.success) {
            let dayturn = this.getDay(response.shift.shift_date);
            this.validateForm.patchValue({
              dateTurn: dayturn,
              shiftTurn: null,
              observationsTurn: response.shift.comments,
            });
            this.showEditDiv = true;
            this.checkShifts(dayturn);
          }
        },
        (err) => {
          let message = {
            en: 'There was an error getting the information',
            es: 'Se produjo un error al obtener la información',
            fr: 'Une erreur s\'est produite lors de l\'obtention des informations'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  getDay(date) {
    let today = date ? new Date(date) : new Date();
    let month = today.getMonth()+1;
    let day = today.getDate();
    return today.getFullYear() +'-'+ (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);
  }

  getShiftHour(date) {
    return new Date(date).getUTCHours();
  }

  checkShifts(ev) {
    this.validateForm.patchValue({shiftTurn: null});
    let date = new Date(ev.replace(/-/g, '/') + ' 00:00:00')
    let today = new Date();
    let diff = Math.ceil((date.getTime() - today.getTime())/ (1000 * 60 * 60 * 24))
    if(diff > 2) {
      this.validateForm.controls['dateTurn'].setErrors({'maxAllowed':true});
      return;
    }
    if(diff < -0.5) {
      this.validateForm.controls['dateTurn'].setErrors({'incorrect':true});
      return;
    }
    let dateDay = date.getDay();
    if(!this.store.opening_hours.day.includes(dateDay)) {
      this.validateForm.controls['dateTurn'].setErrors({'noShifts':true});
      return;
    }
    let check =  diff > -0.5 && diff <= 0 ? true : false;
    let todayHour = today.getHours()*60 + today.getMinutes();
    this.isSpinningStores = true;
    this.userService
      .getShifts(this.storeId, date.getTime())
      .pipe(
        finalize(() => {
          this.isSpinningStores = false;
        } )
      )
      .subscribe((response: any) => {
        this.shifts = [];
        for (let i = 0; i < response.slots.length; i++) {
          if (response.slots[i].available) {
            let slot_hour;
            let hour = response.slots[i].slot_hour_start.split(':');
            if(hour[0] === '12') {
              slot_hour = parseInt(hour[0]) - (response.slots[i].slot_hour_start.includes('AM') ? 12 : 0);
            } else {
              slot_hour = parseInt(hour[0]) + (response.slots[i].slot_hour_start.includes('PM') ? 12 : 0);
            }
            slot_hour = slot_hour*60 + parseInt(hour[1]);
            if((check && todayHour <= slot_hour) || check === false) {
              this.shifts.push({
                number: response.slots[i].slot_hour_start,
                hours: response.slots[i].slot_hours,
              });
            }
          }
        }
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was an error checking shifts',
          es: err.error.message.es || 'Hubo un error al verificar los turnos',
          fr: err.error.message.fr || 'Une erreur s\'est produite lors de la vérification des shifts'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  deleteTurn(id) {
    this.userService.deleteShift(id).subscribe(
      (response: any) => {
        this.dataSet = this.dataSet.filter(function (el) {
          return el._id != id;
        });
        this.dataSource = this.dataSet.slice(0, elementsPerPage);
        let message = {
          en: 'Shift deleted',
          es: 'Turno eliminado',
          fr: 'Shift supprimé'
        };
        this.blService.createMessage('success', message);
      },
      (err) => {
        let message = {
          en: 'Shift not deleted',
          es: 'Turno no eliminado',
          fr: 'Shift non supprimé'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  cancel() {
    this.showEditDiv = false;
  }

  SubmitForm() {
    let prevDate = (this.validateForm.controls['dateTurn'].value + " " + this.validateForm.controls['shiftTurn'].value.number).replace(/-/g,'/');
    let date = new Date(prevDate).getTime();
    this.userService
      .putShift(
        date,
        this.validateForm.controls['observationsTurn'].value,
        this.shiftId
      )
      .pipe(
        finalize(() => {
          //TODO
        })
      )
      .subscribe(
        (response: any) => {
          if (response.success) {
            let message = {
              en: 'Shift updated',
              es: 'Turno actualizado',
              fr: 'Shift mis à jour'
            };
            this.blService.createMessage('success', message);
            this.checkShiftsUser();
            this.showEditDiv = false;
          } else {
            let message = {
              en: 'Shift not updated',
              es: 'Turno no actualizado',
              fr: 'Shift non mis à jour'
            };
            this.blService.createMessage('error', message);
          }
        },
        (err) => {
          let message = {
            en: 'Shift not updated',
            es: 'Turno no actualizado',
            fr: 'Shift non mis à jour'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  resetPaginator() {
    this.manualPage = 1;
    this.offset = 0;
    if (this.paginator) {
      this.paginator.pageIndex = 0;
    }
    this.goToPage();
  }

  // Change paginations values when selected by user
  onChangePage(ev) {
    this.currentPage = ev.pageIndex;
    this.offset = elementsPerPage * this.currentPage;
    this.manualPage = this.currentPage + 1;
    this.goToPage();
  }

  // Change pagination page manually
  updateManualPage(ev) {
    this.currentPage = ev - 1;
    this.offset = elementsPerPage * this.currentPage;
    this.paginator.pageIndex = ev - 1;
    this.goToPage();
  }

  checkManual(code: string) {
    if (!this.location) {
      let message = {
        en: 'Is necessary to know your location before manual check',
        es: 'Es necesario saber su localización antes de realizar el registro manual',
        fr: 'Est nécessaire de connaître votre localization avant la vérification manuelle'
      };
      this.blService.createMessage('error', message);
      this.router.navigateByUrl('/user/user-location');
      return;
    }
    this.userService
      .checkManual(code, this.location, new Date().getTime())
      .subscribe(
        (response: any) => {
          let message = {
            en: 'Checked in',
            es: 'Registrado',
            fr: 'Enregistré'
          };
          this.blService.createMessage('success', message);
          this.dataSetGlobal.filter(function (item) {
            return item.shift_code === code;
          })[0].shift_checked = true;
          this.dataSource.filter(function (item) {
            return item.shift_code === code;
          })[0].shift_checked = true;
        },
        (err) => {
          let message = {
            en: 'There was an error checkin this shift manually',
            es: 'Hubo un error al registrar este turno manualmente',
            fr: 'Une erreur s\'est produite lors de l\'enregistrement manuel de ce shift'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  disableManual(el) {
    let date = el.shift_date;
    let size = el.establishment.slot_size;
    let retVal: boolean = true;
    let today = new Date().getTime();
    let targetDate = new Date(date).getTime();
    if (
      (targetDate - 15 * 60 * 1000) < today &&
      (targetDate + (size) * 60 * 1000) > today
    ) {
      retVal = false;
    }
    return retVal;
  }

  openDialogDeleteShift(id: string) {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      data: {
      title: this.TitleSureText[this.languageService.language],
      data: this.AreSureText[this.languageService.language],
      noText: this.NoSureText[this.languageService.language],
      yesText: this.YesSureText[this.languageService.language]
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'yes') {
        this.deleteTurn(id);
      }
    });
  }

  checkDays(days) {
    if(!days) return;
    if(days.length === 7) return this.AllDaysText[this.languageService.language]
    let retVal = [];
    days.forEach(element => {
      retVal.push(this.day_name[element][this.languageService.language]);
    });
    return retVal.join(', ');
  }

  goToPage() {
    this.dataSource = this.dataSet.slice(
      this.offset,
      elementsPerPage + this.offset
    );
  }

  showAllShifts() {
    this.showAll = true;
    this.dataSet = this.dataSetGlobal;
    this.total = this.dataSet.length;
    this.dataSource = this.dataSet.slice(0, elementsPerPage);
  }

  showLessShifts() {
    this.showAll = false;
    let today = new Date().getTime();
    this.dataSet = this.dataSetGlobal.filter(function(item) {
      let item_date = new Date(item.shift_date).getTime() + (item.establishment.slot_size) * 60 * 1000;
      return item_date > today; 
    });
    this.total = this.dataSet.length;
    this.dataSource = this.dataSet.slice(0, elementsPerPage);
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

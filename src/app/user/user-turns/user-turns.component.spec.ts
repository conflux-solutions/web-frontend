import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTurnsComponent } from './user-turns.component';

describe('UserTurnsComponent', () => {
  let component: UserTurnsComponent;
  let fixture: ComponentFixture<UserTurnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTurnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTurnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

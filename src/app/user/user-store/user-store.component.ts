import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserResultsService } from '../../services/user-results.service';
import { UserService } from '../../services/user.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { location } from '../../models/types'
import { UserAuthService } from '../../services/user-auth.service';
import { LanguageService } from '../../services/language.service';

declare var ol: any;

@Component({
  selector: 'app-user-store',
  templateUrl: './user-store.component.html',
  styleUrls: ['./user-store.component.css'],
})
export class UserStoreComponent implements OnInit, OnDestroy {
  storeId: string;
  showTurn: string = 'initial';
  showResults: boolean = false;
  validateForm: FormGroup;
  store: any;
  shifts: any = [];
  finalDate: number;
  finalShift: { hours: ''; number: 0 };
  finalComment: string;
  shiftCode: string;
  results: any[] = [];
  isSpinningStores: boolean = false;
  isSpinningStore: boolean = true;
  locationSub: Subscription;
  location: location;
  minDate: string = "2013-12-25";
  maxDate: string = "2113-12-25";
  private userSub: Subscription;
  private userOpenSub: Subscription;
  logged: boolean = false;
  map: any;
  graphicLayer: any;
  desktop = 'true';

  day_name = [
    {en: 'Sunday', es: 'Domingo', fr: 'Dimanche'},
    {en: 'Monday', es: 'Lunes', fr: 'Lundi'},
    {en: 'Tuesday', es: 'Martes', fr: 'Mardi'},
    {en: 'Wednsday', es: 'Miércoles', fr: 'Mercredi'},
    {en: 'Thursday', es: 'Jueves', fr: 'Jeudi'},
    {en: 'Friday', es: 'Viernes', fr: 'Vendredi'},
    {en: 'Saturday', es: 'Sábado', fr: 'Samedi'},
  ];

  //Texts
  ReserveShiftText = {en: 'Reserve your shift', es: 'Reserve su turno', fr: 'Réservez votre shift'};
  LoginText = {en: 'Log in to book a shift', es: 'Inicie sesión para reservar un turno', fr: 'Connectez-vous pour réserver un quart de travail'};
  ReserveText = {en: 'RESERVE YOUR SHIFT', es: 'RESERVE SU TURNO', fr: 'RÉSERVEZ VOTRE SHIFT'};
  DateText = {en: 'DATE', es: 'FECHA', fr: 'DATE'};
  ShiftText = {en: 'SHIFT', es: 'TURNO', fr: 'SHIFT'};
  ObservationsText = {en: 'OBSERVATIONS', es: 'OBSERVACIONES', fr: 'OBSERVATIONS'};
  CancelText = {en: 'CANCEL', es: 'CANCELAR', fr: 'ANNULER'};
  ConfirmText = {en: 'CONFIRM', es: 'CONFIRMAR', fr: 'CONFIRMER'};
  EditText = {en: 'EDIT', es: 'EDITAR', fr: 'ÉDITER'};
  SummaryText = {en: 'SUMMARY', es: 'RESUMEN', fr: 'SOMMAIRE'};
  EstablishmentText = {en: 'ESTABLISHMENT', es: 'ESTABLECIMIENTO', fr: 'ÉTABLISSEMENT'};
  ShiftConfirmedText = {en: 'Shift confirmed with code', es: 'Turno confirmado con código', fr: 'Shift confirmé avec code'};
  HomeText = {en: 'HOME', es: 'INICIO', fr: 'ACCUEIL'};
  GobackText = {en: 'GO BACK', es: 'ATRÁS', fr: 'RETOURNER'};
  CheckTurnsText = {en: 'MY SHIFTS', es: 'MIS TURNOS', fr: 'MY SHIFTS'};
  ProblemShiftText = {en: 'Problem setting shift', es: 'No se pudo establecer el turno', fr: 'Le changement n\'a pas pu être établi'};
  CloseText = {en: 'Close', es: 'Cerrar', fr: 'Fermer'};
  LessOccupationText = {en: 'Less Occupation', es: 'Menor ocupación', fr: 'Moins d\'occupation'};
  NearMeText = {en: 'Near me', es: 'Cercanía', fr: 'Proche de moi'};
  NameText = {en: 'Name', es: 'Nombre', fr: 'Nom'};
  AddressText = {en: 'Address', es: 'Dirección', fr: 'Adresse'};
  PhoneText = {en: 'Phone', es: 'Teléfono', fr: 'Téléphone'};
  OpenText = {en: 'Open', es: 'Abierto', fr: 'Ouvert'};
  DatePreviousText = {en: 'This date is previous to actual date', es: 'El día es anterior a hoy', fr: 'Cette date est antérieure à la date réelle'};
  DateFutureErrText = {en: 'Date must be less than 2 days in the future', es: 'La fecha seleccionada no debe superar dos días en el futuro', fr: 'La date doit être inférieure à 2 jours dans le futur'};
  NoShiftsErrText = {en: 'On this date this establishment is closed', es: 'En esta fecha este establecimiento está cerrado', fr: 'A cette date cet établissement est fermé'};
  TodayText = {en: 'Today', es: 'Hoy', fr: 'Aujourd\'hui'};
  AllDaysText = {en: 'All days', es: 'Todos los días', fr: 'Tous les jours'};

  constructor(
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private resultsService: UserResultsService,
    private userService: UserService,
    private blService: BusinessLogicService,
    private router: Router,
    private userAuthService: UserAuthService,
    public languageService: LanguageService
  ) {
    this.resultsService.oppenedStores.next('false');
    this.validateForm = this.fb.group({
      dateTurn: [this.getDay(null), [Validators.required]],
      shiftTurn: [null, [Validators.required]],
      observationsTurn: [''],
    });
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          this.location = res;
          if(this.results.length === 1) {
            this.results[0].location['distance_to_origin'] = this.checkDistance(this.results[0].location);
          }
        } else {
          //this.blService.createMessage('error','Enable GPS location');
        }
      }
    );
    this.blService.setCurrentLocation();
    this.userSub = this.userAuthService.user.subscribe(user => {
      if(!!user) {
        this.logged = true;
      } 
    });
    this.userOpenSub = this.resultsService.opennedMenu.subscribe((openned) => {
      this.desktop = (document.documentElement.clientWidth || window.screen.width) > (600 + (openned ? 300 : 0)) ? 'true' : 'false';
    })
  }

  ngOnInit(): void {
    //set map
    this.map = new ol.Map({
      target: 'map2',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([parseFloat(this.blService.default_longitude), parseFloat(this.blService.default_latitude)]),
        zoom: 17
      })
    });
    //set results
    this.results = this.resultsService.getResults();
    this.storeId = this.activeRoute.snapshot.params['id'];
    if(this.results.length > 0) {
      this.loadStore(this.storeId);
      this.orderByOccupation();
    } else {
      this.userService.getStore(this.storeId).subscribe(
        (response: any) => {
          this.results = [response.establishment];
          this.results[0].location['distance_to_origin'] = this.checkDistance(this.results[0].location); 
          this.results[0].location['units'] = 'km';
          this.results[0]['percentage'] = Math.floor(this.results[0].current_affluences*100/this.results[0].max_affluences_allowed);
          this.loadStore(this.storeId);
        },
        err => {
          let message = {
            en: err.error.message.en || 'There was a problem loading store',
            es: err.error.message.es || 'Hubo un problema al cargar el establecimiento',
            fr: err.error.message.fr || 'Un problème est survenu lors du chargement de l\'établissement'
          };
          this.blService.createMessage('error', message);
          this.router.navigateByUrl('user/user-home');
        }
      )
    }
    this.minDate = this.getDay(null);
  }

  getDay(date) {
    let today = date ? new Date(date) : new Date();
    let month = today.getMonth()+1;
    let day = today.getDate();
    return today.getFullYear() +'-'+ (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);
  }

  checkDistance(location) {
    if(this.location) {
      return this.calculateDistance(location.latitude, location.longitude, this.location.latitude, this.location.longitude);
    } else {
      return '';
    }
  }

  goBack() {
    this.resultsService.oppenedStores.next('true');
    this.router.navigateByUrl('/user/user-home');
  }

  calculateDistance = (lat1, lon1, lat2, lon2, unit="K") => {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * lat1/180;
      var radlat2 = Math.PI * lat2/180;
      var theta = lon1-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344 }
      if (unit=="N") { dist = dist * 0.8684 }
      return dist;
    }
  }

  loadStore(id) {
    this.store = this.results.filter(function (item) {
      return item._id === id;
    })[0];
    this.setCenterMap({ latitude: parseFloat(this.store.location.latitude), longitude: parseFloat(this.store.location.longitude)})
    //let max_date = new Date().getTime() + (this.store.shift_schedule_max_hours*60*60*1000);
    let max_date = new Date().getTime() + (48*60*60*1000);
    this.maxDate = this.getDay(max_date);
    this.showTurn = 'initial';
    this.showResults = false;
    this.isSpinningStore = false;
  }

  setCenterMap(loc) {
    var view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([loc.longitude, loc.latitude]));
    view.setZoom(17);
    if (this.graphicLayer) {
      this.map.removeLayer(this.graphicLayer);
    }
    this.graphicLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.transform([loc.longitude, loc.latitude], 'EPSG:4326', 'EPSG:3857')),
        })]
      }),
      style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "assets/icons/fullStore.svg"
        })
      })
    });
  this.map.addLayer(this.graphicLayer);
  }

  checkShifts(ev) {
    this.validateForm.patchValue({shiftTurn: null});
    let date = new Date(ev.replace(/-/g, '/') + ' 00:00:00');
    let today = new Date();
    let diff = Math.ceil((date.getTime() - today.getTime())/ (1000 * 60 * 60 * 24))
    if(diff > 2) {
      this.validateForm.controls['dateTurn'].setErrors({'maxAllowed':true});
      return;
    }
    if(diff < -0.5) {
      this.validateForm.controls['dateTurn'].setErrors({'incorrect':true});
      return;
    }
    let dateDay = date.getDay();
    if(!this.store.opening_hours.day.includes(dateDay)) {
      this.validateForm.controls['dateTurn'].setErrors({'noShifts':true});
      return;
    }
    let check = diff > -0.5 && diff <= 0 ? true : false;
    let todayHour = today.getHours()*60 + today.getMinutes();
    this.isSpinningStores = true;
    this.userService
      .getShifts(this.storeId, date.getTime())
      .pipe(
        finalize(() => {
          this.isSpinningStores = false;
        } )
      )
      .subscribe((response: any) => {
        this.shifts = [];
        for (let i = 0; i < response.slots.length; i++) {
          if (response.slots[i].available) {
            let slot_hour;
            let hour = response.slots[i].slot_hour_start.split(':');
            if(hour[0] === '12') {
              slot_hour = parseInt(hour[0]) - (response.slots[i].slot_hour_start.includes('AM') ? 12 : 0);
            } else {
              slot_hour = parseInt(hour[0]) + (response.slots[i].slot_hour_start.includes('PM') ? 12 : 0);
            }
            slot_hour = slot_hour*60 + parseInt(hour[1]);
            if((check && todayHour <= slot_hour) || check === false) {
              this.shifts.push({
                number: response.slots[i].slot_hour_start,
                hours: response.slots[i].slot_hours,
              });
            }
          }
        }
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was an error checking shifts',
          es: err.error.message.es || 'Hubo un error al verificar los turnos',
          fr: err.error.message.fr || 'Une erreur s\'est produite lors de la vérification des shifts'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  checkDay(days) {
    var today = new Date().getDay();
    if (days.includes(today)) {
      return this.TodayText[this.languageService.language];
    } else {
      let close_day = 10;
      days.forEach((item) => {
        let close = item-today;
        close_day = close > 0 && close < close_day ? close : close_day; 
      })
      return this.day_name[close_day+today][this.languageService.language];
    }
  }

  checkDays(days) {
    if(!days) return;
    if(days.length === 7) return this.AllDaysText[this.languageService.language]
    let retVal = [];
    days.forEach(element => {
      retVal.push(this.day_name[element][this.languageService.language]);
    });
    return retVal.join(', ');
  }

  checkHours(hours) {
    if(!hours) return '';
    var openHour = new Date(hours.open_hour).getUTCHours();
    var closeHour = new Date(hours.close_hour).getUTCHours();
    return (openHour > 12 ? openHour-12 + ' PM' : openHour === 12 ? openHour + ' PM' : openHour + ' AM') + ' - ' +  (closeHour > 12 ? closeHour-12 + ' PM' : closeHour === 12 ? closeHour + ' PM' : closeHour + ' AM');
  }

  reserve() {
    if(this.logged) {
    this.showTurn = 'options';
    this.checkShifts(this.validateForm.controls['dateTurn'].value);
    } else {
      this.router.navigate(['user-auth/'], { queryParams: { id: this.storeId } }); 
    }
  }

  cancel() {
    this.showTurn = 'initial';
  }

  goHome() {
    this.router.navigateByUrl('/user/user-home');
  }

  goTurns() {
    this.router.navigateByUrl('/user/user-turns');
  }

  initialSubmitForm() {
    this.finalDate = this.validateForm.controls['dateTurn'].value;
    this.finalShift = this.validateForm.controls['shiftTurn'].value;
    this.finalComment = this.validateForm.controls['observationsTurn'].value
    this.showTurn = 'summary';
  }

  SubmitForm() {
    let prevDate = (this.finalDate + " " + this.finalShift.number).replace(/-/g,'/');
    let date = new Date(prevDate).getTime();
    this.userService
      .postShift(
        date,
        this.finalComment,
        this.store._id
      )
      .pipe(
        finalize(() => {
          //TODO
        } )
      )
      .subscribe((response: any) => {
        if (response.success) {
          this.shiftCode = response.shift.shift_code;
          this.showTurn = 'checked';
        } else {
          this.showTurn = 'notChecked';
        }
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was an error submitting the form',
          es: err.error.message.es || 'Hubo un error al enviar el formulario',
          fr: err.error.message.fr || 'Il y a eu une erreur en soumettant le formulaire'
        };
        this.blService.createMessage('error', message);
        this.showTurn = 'notChecked';
      }
    );
  }
  
  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
    if (this.userSub) this.userSub.unsubscribe();
    if (this.userOpenSub) this.userOpenSub.unsubscribe();
  }

  getColorPercentage(percentage:number){
    let retVal = '#ffb3b3';
    if(percentage < 50) retVal = '#FFF2E5';
    if(percentage < 10) retVal = '#80ff80';
    return retVal;
  }

  getColorPercentageStar(percentage:number){
    let retVal = '#ff1a1a';
    if(percentage < 50) retVal = '#FFA143';
    if(percentage < 10) retVal = '#00b300';
    return retVal;
  }

  getColorPercentageFilter(percentage:number){
    let retVal = 'invert(27%) sepia(67%) saturate(7038%) hue-rotate(350deg) brightness(101%) contrast(109%)';
    if(percentage < 50) retVal = 'invert(78%) sepia(48%) saturate(2610%) hue-rotate(327deg) brightness(104%) contrast(101%)';
    if(percentage < 10) retVal = 'invert(11%) sepia(49%) saturate(3174%) hue-rotate(98deg) brightness(99%) contrast(106%)';
    return retVal;
  }

  orderByLocation() {
    this.results = this.results.sort(
      function(a,b) {
        return a.location.distance_to_origin-b.location.distance_to_origin;
      }
    );
    this.resultsService.setResults(this.results);
  }

  orderByOccupation() {
    this.results = this.results.sort(function (a, b) {
      return a.percentage - b.percentage;
    });
    this.resultsService.setResults(this.results);
  }
}

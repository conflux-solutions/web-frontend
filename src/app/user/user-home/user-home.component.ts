import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UserResultsService } from '../../services/user-results.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { location } from '../../models/types';
import { ParamsService } from '../../services/params.service';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css'],
})
export class UserHomeComponent implements OnInit, OnDestroy {
  showResults: boolean = false;
  numberResults: number = 0;
  searchText: string = '';
  parcialSearchText: string = '';
  searchValue: string;
  results: any = [];
  isSpinning: boolean = false;
  isSpinningParams: boolean = true;
  location: location;
  locationSub: Subscription;
  radius: string = '2km';
  arrayRadius: string[];
  categories: any;
  searchCategory: string;

  associate_icons = [
    { id: 1, title: 'Health', image: 'assets/icons/health.svg' },
    { id: 2, title: 'Restaurant', image: 'assets/icons/Restaurante.svg' },
    { id: 3, title: 'Bank', image: 'assets/icons/pig.svg' },
    { id: 4, title: 'Market', image: 'assets/icons/market.svg' },
    { id: 4, title: 'Supermarket', image: 'assets/icons/centro-comercial.svg' },
    { id: 5, title: 'Transport', image: 'assets/icons/transport.svg' },
    { id: 6, title: 'Library', image: 'assets/icons/biblioteca.svg' },
    { id: 7, title: 'Church', image: 'assets/icons/religion.svg' },
    { id: 8, title: 'Coffee', image: 'assets/icons/food.svg' },
    { id: 9, title: 'Cloths', image: 'assets/icons/ropa.svg' },
    { id: 10, title: 'Office', image: 'assets/icons/oficina.svg' },
    { id: 11, title: 'Workshop', image: 'assets/icons/servicios_generales.svg' },
    { id: 12, title: 'More', image: 'assets/icons/plus.svg' },
    { id: 13, title: 'Barber Shop', image: 'assets/icons/peluqueria.svg' },
    { id: 14, title: 'Sports', image: 'assets/icons/deporte.svg' },
    { id: 15, title: 'Entertainment', image: 'assets/icons/entretenimiento.svg' },
    { id: 16, title: 'Transit and transport', image: 'assets/icons/transport.svg' },
  ];

  day_name = [
    {en: 'Sunday', es: 'Domingo', fr: 'Dimanche'},
    {en: 'Monday', es: 'Lunes', fr: 'Lundi'},
    {en: 'Tuesday', es: 'Martes', fr: 'Mardi'},
    {en: 'Wednsday', es: 'Miércoles', fr: 'Mercredi'},
    {en: 'Thursday', es: 'Jueves', fr: 'Jeudi'},
    {en: 'Friday', es: 'Viernes', fr: 'Vendredi'},
    {en: 'Saturday', es: 'Sábado', fr: 'Samedi'},
  ];

  //Texts
  WelcomeText = {en: 'Welcome to Conflux', es: 'Bienvenido a Conflux', fr: 'Bienvenue chez Conflux'};
  SearchText = {en: 'Search for the place you want to visit', es: 'Busque el lugar que quiere visitar', fr: 'Recherchez le lieu que vous souhaitez visiter'};
  CategoryText = {en: 'Choose a category', es: 'Seleccione una categoría', fr: 'Choisissez une catégorie'};
  ResultsForText = {en: 'results for', es: 'resultados para', fr: 'résultats pour'};
  LessOccupationText = {en: 'Less Occupation', es: 'Menor ocupación', fr: 'Moins d\'occupation'};
  NearMeText = {en: 'Near me', es: 'Cercanía', fr: 'Proche de moi'};
  TodayText = {en: 'Today', es: 'Hoy', fr: 'Aujourd\'hui'};
  TiendaEjText = {en: 'Store', es: 'Establecimiento', fr: 'Établissement'};
  searchButtonText = {en: 'Search', es: 'Buscar', fr: 'Chercher'};

  constructor(
    private router: Router,
    private userService: UserService,
    private resultsService: UserResultsService,
    private blService: BusinessLogicService,
    private paramsService: ParamsService,
    public languageService: LanguageService,
    private activeRoute: ActivatedRoute
  ) {
    this.searchValue = this.activeRoute.snapshot.queryParamMap.get('search') || null;
    this.searchCategory = this.activeRoute.snapshot.queryParamMap.get('category') || null;
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          this.location = res;
          if(this.searchValue) {
            this.searchText = this.searchValue;
            this.searchPlace();
          }
          if(this.searchCategory) {
            this.setShops(this.searchCategory);
          }
        } else {
          //this.blService.createMessage('error','Enable GPS location');
        }
      } 
    );
    this.blService.setCurrentLocation();
    this.paramsService.getShopsParams().subscribe(
      (response: any) => {
        let params = response.params;
        this.categories = params.filter(function (item) {
          return item.name === 'Categories';
        })[0].value;
        this.arrayRadius = params.filter(function (item) {
          return item.name === 'DistanceEstablishment';
        })[0].value;
        this.isSpinningParams = false;
      }
    );
  }

  ngOnInit(): void {
    this.results = this.resultsService.getResults();
    if(this.results.length > 0){
      this.showResults = true;
      this.numberResults = this.results.length;
      this.parcialSearchText = this.resultsService.getsearchText();
    }
  }

  setShops(title: string) {
    if(!this.location) {
      let message = {
        en: 'We need to know your location to search stores close to you',
        es: 'Necesitamos saber su ubicación para buscar los establecimientos cerca de usted',
        fr: 'Nous avons besoin de connaître votre position pour rechercher des établissements près de chez vous'
      };
      this.blService.createMessage('info', message);
      this.router.navigateByUrl('/user/user-location');
    }
    this.searchText = title;
    this.isSpinning = true;
    this.userService
      .getStoresByLocByName(this.location, title, this.radius)
      .pipe(
        finalize(() => {
          this.isSpinning = false;
        })
      )
      .subscribe(
        (response: any) => {
          this.numberResults = response.total;
          this.results = response.values.slice(0, 20); //Max 20 resultados
          this.results.forEach(element => {
            element.percentage = Math.floor(element.current_affluences*100/element.max_affluences_allowed);
          });
          this.orderByLocation();
          for (let i = 0; i < this.results.length; i++) {
            let i_title = this.results[i].category;
            this.results[i]['image'] = this.associateIcon(i_title);
          }
          this.resultsService.setResults(this.results);
          this.resultsService.setsearchText(this.searchText);
          this.parcialSearchText = this.searchText;
          this.showResults = true;
        },
        (err) => {
          let message = {
            en: 'There was an error setting shops',
            es: 'Hubo un error al establecer establecimientos',
            fr: 'Une erreur s\'est produite lors de la définition des établissements'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  getColorPercentage(percentage:number){
    let retVal = '#ffb3b3';
    if(percentage < 50) retVal = '#FFF2E5';
    if(percentage < 10) retVal = '#80ff80';
    return retVal;
  }

  getColorPercentageStar(percentage:number){
    let retVal = '#ff1a1a';
    if(percentage < 50) retVal = '#FFA143';
    if(percentage < 10) retVal = '#00b300';
    return retVal;
  }

  getColorPercentageFilter(percentage:number){
    let retVal = 'invert(27%) sepia(67%) saturate(7038%) hue-rotate(350deg) brightness(101%) contrast(109%)';
    if(percentage < 50) retVal = 'invert(78%) sepia(48%) saturate(2610%) hue-rotate(327deg) brightness(104%) contrast(101%)';
    if(percentage < 10) retVal = 'invert(11%) sepia(49%) saturate(3174%) hue-rotate(98deg) brightness(99%) contrast(106%)';
    return retVal;
  }

  associateIcon(title) {
    return this.associate_icons.filter(function (item) {
      return new RegExp( item.title ,"i").test(title);
    })[0]?.image || 'assets/icons/more.svg';
  }

  checkDay(days) {
    var today = new Date().getDay();
    if (days.includes(today)) {
      return this.TodayText[this.languageService.language];
    } else {
      let close_day = 10;
      days.forEach((item) => {
        let close = item-today;
        close_day = close > 0 && close < close_day ? close : close_day; 
      })
      return this.day_name[close_day+today][this.languageService.language];
    }
  }

  searchPlace(){
    if(this.searchText?.length <= 0 || this.searchText?.trim() === '' || !this.searchText) return;
    this.isSpinning = true;
    this.userService
      .searchEstablishment(this.searchText, this.location, this.radius)
      .pipe(
        finalize(() => {
          this.isSpinning = false;
        })
      )
      .subscribe(
        (response: any) => {
          if(response){
            this.numberResults = response.total;
            this.results = response.values.slice(0,20); //Max 20 resultados
            this.results.forEach(element => {
              element.percentage = Math.floor(element.current_affluences*100/element.max_affluences_allowed);
            });
            this.orderByLocation();
            for (let i = 0; i < this.results.length; i++) {
              let i_title = this.results[i].category;
              this.results[i]['image'] = this.associateIcon(i_title);
            }
            this.resultsService.setResults(this.results);
            this.resultsService.setsearchText(this.searchText);
            this.parcialSearchText = this.searchText;
            this.showResults = true;
          }
        },
        (err) => {
          let message = {
            en: 'There was an error searching shops',
            es: 'Hubo un error al buscar establecimientos',
            fr: 'Une erreur s\'est produite lors de la recherche dans les établissements'
          };
          this.blService.createMessage('error', message);
        }
      );
  }

  checkHours(hours) {
    var openHour = new Date(hours.open_hour).getUTCHours();
    var closeHour = new Date(hours.close_hour).getUTCHours();
    return (openHour > 12 ? openHour-12 + ' PM' : openHour === 12 ? openHour + ' PM' : openHour + ' AM') + ' - ' +  (closeHour > 12 ? closeHour-12 + ' PM' : closeHour === 12 ? closeHour + ' PM' : closeHour + ' AM');
  }

  backToCategories() {
    this.showResults = false;
  }

  loadMap(id) {
    this.router.navigateByUrl('/user/user-store/' + id);
  }

  orderByLocation() {
    this.results = this.results.sort(function (a, b) {
      return a.location.distance_to_origin - b.location.distance_to_origin;
    });
    this.resultsService.setResults(this.results);
  }

  orderByOccupation() {
    this.results = this.results.sort(function (a, b) {
      return a.percentage - b.percentage;
    });
    this.resultsService.setResults(this.results);
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

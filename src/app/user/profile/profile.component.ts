import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { passwordValidator } from '../../validators/password.validator';
import { UserService } from '../../services/user.service';
import { finalize } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  validateForm: FormGroup;
  isSpinningSave: boolean = false;

  //Texts
  ChangePassword = {en: 'Change Password', es: 'Cambiar contraseña', fr: 'Changer mot de passe'};
  NewPassword = {en: 'New Password', es: 'Nueva contraseña', fr: 'Nouvelle mot de passe'};
  ConfirmPassword = {en: 'Confirm Password', es: 'Confirmar contraseña', fr: 'Confirmer mot de passe'};
  ChangeText = {en: 'Change', es: 'Cambiar', fr: 'Changer'};

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private blService: BusinessLogicService,
    private router: Router,
    public languageService: LanguageService
  ) {}

  ngOnInit() {
    this.validateForm = this.fb.group({
      password: [null, [Validators.required, Validators.minLength(8), passwordValidator]],
      password2: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    let password = this.validateForm.controls['password'].value;
    let password2 = this.validateForm.controls['password2'].value;
    if(password !== password2) {
      let message = {
        en: 'Passwords are not equal',
        es: 'Las contraseñas no son iguales',
        fr: 'Les mots de passe ne sont pas égaux'
      };
      this.blService.createMessage('error', message);
      return;
    }
    this.isSpinningSave = true;
    this.userService.putPassword(password).pipe(
      finalize(() => {this.isSpinningSave = false;} )
    ).subscribe(
      success => {
        let message = {
          en: 'Your information has been updated',
          es: 'Su información ha sido actualizada',
          fr: 'Vos informations ont été mises à jour'
        };
        this.blService.createMessage('success', message);
        this.router.navigateByUrl('/user/user-home');
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was an error and your information has not been updated',
          es: err.error.message.es || 'Hubo un problema y su información no ha sido actualizada',
          fr: err.error.message.fr || 'Il y a eu une erreur et vos informations n\'ont pas été mises à jour'
        };
        this.blService.createMessage('error', message);
      }
    );
  }
}

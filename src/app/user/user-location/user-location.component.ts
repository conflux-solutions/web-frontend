import { Component, OnInit } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { location, place } from '../../models/types';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GoogleMapService } from '../../services/googlemaps.service';
import { debounceTime } from 'rxjs/operators';
import { ParamsService} from '../../services/params.service';
import { LanguageService } from '../../services/language.service';

declare var ol: any;

@Component({
  selector: 'app-user-location',
  templateUrl: './user-location.component.html',
  styleUrls: ['./user-location.component.css'],
})
export class UserLocationComponent implements OnInit {
  zoom = 15;
  isSpinningSave: boolean = false;
  location: location = null;
  locationSub: Subscription;
  protectedAddress: boolean = false;
  validateForm: FormGroup;
  country: place = null;
  isSpinningMap: boolean = false;
  addressShow: boolean = true;
  addresses: any[] = [];
  allCountries: place[] = [];
  countries: place[] = [];
  countryInput: Subject<string> = new Subject<string>();
  cityInput: Subject<string> = new Subject<string>();
  addressInput: Subject<string> = new Subject<string>();
  isSpinningCountry: boolean = true;
  isSpinningLocation: boolean = true;
  globalLatitude: number = parseFloat(this.blService.default_latitude);
  globalLongitude: number = parseFloat(this.blService.default_longitude);
  map: any;
  graphicLayer: any;
  locationTimeout;

  //Texts
  CheckLocation = {en: 'CHECK LOCATION', es: 'UBICAR', fr: 'LOCALISER'};
  GetDeviceText = {en: 'Get device location', es: 'Ubicación del dispositivo', fr: 'Emplacement de l\'appareil'};
  CountryText = {en: 'Country', es: 'País', fr: 'Pays'};
  CityText = {en: 'City', es: 'Ciudad', fr: 'Ville'};
  StreetText = {en: 'District', es: 'Zona', fr: 'District'};
  OptionsText = {en: 'options', es: 'opciones', fr: 'd\'options'};
  MoreText = {en: 'More', es: 'Más', fr: 'Plus'};
  LessText = {en: 'Less', es: 'Menos', fr: 'Moins'};
  SetLocationText = {en: 'Set this location', es: 'Establecer esta ubicación', fr: 'Définir cet emplacement'};

  constructor(
    private blService: BusinessLogicService,
    private fb: FormBuilder,
    private paramsService: ParamsService,
    private googleService: GoogleMapService,
    public languageService: LanguageService
  ) {
    this.validateForm = this.fb.group({
      country: [null, [Validators.required]],
      city: [null, [Validators.required]],
      address: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([this.globalLongitude, this.globalLatitude]),
        zoom: 17
      })
    });
    this.mapClicked();
    this.locationTimeout = setTimeout(() => {
      this.isSpinningLocation = false;
      let message = {
        en: 'Location is taking longer than expected. Check if GPS is enabled',
        es: 'La Localización está tardando más de lo esperado. Compruebe si el GPS está habilitado',
        fr: 'La localization prend plus de temps que prévu. Vérifiez si le GPS est activé'
      };
      this.blService.createMessage('error', message);
    }, 5000);
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          let message = {
            en: 'Located',
            es: 'Localizado',
            fr: 'Localisé'
          };
          this.blService.createMessage('success', message);
          clearTimeout(this.locationTimeout);
          this.isSpinningLocation = false;
          this.location = res;
          let countryName = this.blService.local_display_name.country;
          let code = this.blService.local_display_name.country_code;
          let cityName = this.blService.local_display_name.city;
          let addressName = this.blService.local_display_name.address;
          this.protectedAddress = true;
          this.validateForm.patchValue({
            country: countryName,
            city: cityName,
            address: addressName,
          });
          this.country = { name: countryName, code: code };
          this.globalLatitude = parseFloat(res.latitude);
          this.globalLongitude = parseFloat(res.longitude);
          this.setCenterMap({latitude: this.globalLatitude, longitude: this.globalLongitude});
        } else {
          //this.blService.createMessage('error','Enable GPS location');
        }
      }
    );
    this.blService.setCurrentLocation();
    this.paramsService.geCountryParams().subscribe(
      (response: any) => {
        if (response.success) {
          this.allCountries = response.countries;
          this.isSpinningCountry = false;
        }
      },
      (err) => {
        let message = {
          en: 'There was an error loading countries',
          es: 'Hubo un error al cargar los países',
          fr: 'Une erreur s\'est produite lors du chargement des pays'
        };
        this.blService.createMessage('error', message);
        this.isSpinningCountry = false;
      }
    );
    this.initKeyUpListenerAddress();
  }

  initKeyUpListenerAddress() {
    this.countryInput.pipe(debounceTime(700)).subscribe((data: string) => {
      this.countries = [];
      let completed = 4;
      let last_get = 0;
      let compare = this.validateForm.controls['country'].value
        .trim()
        .toLowerCase();
      for (let i = 0; i < this.allCountries.length; i++) {
        if (this.allCountries[i].name.toLowerCase().includes(compare)) {
          this.countries.push(this.allCountries[i]);
          last_get = i;
          completed -= 1;
        }
        if (completed <= 0) break;
      }
      if (this.protectedAddress) {
        this.protectedAddress = false;
        return;
      }
      if (completed >= 3) {
        this.validateForm.patchValue({ city: null, address: null });
        if (!(compare === this.allCountries[last_get].name.toLowerCase())) {
          this.validateForm.controls['country'].setErrors({ incorrect: true });
        }
      }
    });

    this.addressInput.pipe(debounceTime(700)).subscribe((data: string) => {
      let address = this.validateForm.controls['address'].value?.trim();
      let city = this.validateForm.controls['city'].value?.trim();
      let country = this.validateForm.controls['country'].value.trim();
      if (address && country) {
        this.googleService
          .getAutocompleteNominatim(address, city, country)
          .subscribe((response: any) => {
            this.addresses = [];
            this.addresses.push(...response);
          });
      }
    });
  }

  setFirstAddress() {
    this.validateForm.patchValue({
      address: this.addresses[0]?.display_name
    })
  }

  mapClicked() {
    this.map.on('click', 
    (args) => {
      var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      let location = {latitude: lonlat[1], longitude: lonlat[0]};
      this.updateTextLocation(location);
      this.setCenterMap(location);
    });
  }

  updateTextLocation(location) {
    this.location = location;
    this.googleService
        .getAdminLevelByLocation(this.location)
        .subscribe((response: any) => {
          if (response.place_id) {
            let countryName = response.address.country;
            let code = response.address.country_code;
            let cityName = this.blService.getCity(response.address);
            let addressName = response.display_name;
            this.protectedAddress = true;
            this.validateForm.patchValue({
              country: countryName,
              city: cityName,
              address: addressName,
            });
            this.country = { name: countryName, code: code };
          }
      });
  }

  setCenterMap(loc) {
    var view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([loc.longitude, loc.latitude]));
    view.setZoom(17);
    if (this.graphicLayer) {
      this.map.removeLayer(this.graphicLayer);
    }
    this.graphicLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.transform([loc.longitude, loc.latitude], 'EPSG:4326', 'EPSG:3857')),
        })]
      }),
      style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 1],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "assets/icons/fullLocation.svg"
        })
      })
    });
  this.map.addLayer(this.graphicLayer);
  }

  setPlaceID(addressID) {
    if (!addressID || addressID === 'undefined') {
      return;
    }
    let el = this.addresses.filter(function (item) {
      return item.place_id === addressID;
    })[0];
    this.globalLatitude = parseFloat(el.lat);
    this.globalLongitude = parseFloat(el.lon);
    this.setCenterMap({ latitude: this.globalLatitude, longitude: this.globalLongitude});
    this.location = { latitude: el.lat, longitude: el.lon };
  }

  setPlaceStringID() {
    let compare = this.validateForm.controls['address'].value;
    let el = this.addresses.filter(function (item) {
      return item.display_name === compare;
    })[0];
    this.setPlaceID(el.place_id);
  }

  setPlaceString() {
    let address = this.validateForm.controls['address'].value.trim();
    let city = this.validateForm.controls['city'].value.trim();
    let country = this.validateForm.controls['country'].value.trim();
    this.googleService
      .getAutocompleteNominatim(address, city, country)
      .subscribe((response: any) => {
        if (response.length > 0) {
          let el = response[0];
          this.globalLatitude = parseFloat(el.lat);
          this.globalLongitude = parseFloat(el.lon);
          this.setCenterMap({ latitude: this.globalLatitude, longitude: this.globalLongitude});
          this.location = {
            latitude: el.lat,
            longitude: el.lon,
          };
        }
      });
  }

  changed(value) {
    switch (value) {
      case 'country':
        this.countryInput.next();
        break;
      case 'city':
        this.cityInput.next();
        break;
      case 'address':
        this.addressInput.next();
        break;
      default:
        break;
    }
  }

  submitForm() {
    this.blService.setManualLocation(this.location);
  }

  getManualLocation() {
    this.locationTimeout = setTimeout(() => {
      this.isSpinningLocation = false;
      let message = {
        en: 'Location is taking longer than expected. Check if GPS is enabled',
        es: 'La Localización está tardando más de lo esperado. Compruebe si el GPS está habilitado',
        fr: 'La localization prend plus de temps que prévu. Vérifiez si le GPS est activé'
      };
      this.blService.createMessage('error', message);
    }, 5000);
    this.isSpinningLocation = true;
    this.blService.getDeviceLocation();
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user.component';
import { EstablishmentService } from '../services/establishment.service';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatAutocompleteModule} from '@angular/material/autocomplete'; 
import {MatMenuModule} from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserLocationComponent } from './user-location/user-location.component';
import { UserQrComponent } from './user-qr/user-qr.component';
import { UserTurnsComponent } from './user-turns/user-turns.component';
import { UserStoreComponent } from './user-store/user-store.component';
import { QRCodeModule } from 'angularx-qrcode';
import { UserResultsService } from '../services/user-results.service';
import { UserProfileComponent } from '../user/profile/profile.component';
import { UserAuthGuard } from '../auth/user-auth.guard';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { UserAuthInterceptorService } from '../auth/user-auth-interceptor.service'
import { UserService } from '../services/user.service';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { UserDialogComponent } from './user-dialog/user-dialog.component';


const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/user/user-home' },
      { path: 'user-home', component: UserHomeComponent },
      { path: 'user-store/:id', component: UserStoreComponent },
      { path: 'user-qr', component: UserQrComponent, canActivate:[UserAuthGuard] },
      { path: 'profile', component: UserProfileComponent, canActivate:[UserAuthGuard] },
      { path: 'user-location', component: UserLocationComponent },
      { path: 'user-turns', component: UserTurnsComponent, canActivate:[UserAuthGuard] },
      { path: '**', redirectTo: '/user' },
    ],
  },
];

@NgModule({
  declarations: [
    UserComponent,
    UserHomeComponent,
    UserLocationComponent,
    UserQrComponent,
    UserTurnsComponent,
    UserStoreComponent,
    UserProfileComponent,
    UserDialogComponent,
  ],
  imports: [
    ZXingScannerModule,
    CommonModule,
    FormsModule,
    QRCodeModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatMenuModule,
    MatDialogModule,
    MatTableModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatCardModule,
    MatPaginatorModule,
    MatButtonModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    HttpClientModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    EstablishmentService,
    UserResultsService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UserAuthInterceptorService,
      multi: true,
    },
  ],
})
export class UserModule {}

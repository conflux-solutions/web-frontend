import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { EstablishmentService } from '../../services/establishment.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { Subscription } from 'rxjs';
import { location } from '../../models/types';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { debounceTime, finalize } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-user-qr',
  templateUrl: './user-qr.component.html',
  styleUrls: ['./user-qr.component.css'],
})
export class UserQrComponent implements OnInit, OnDestroy {
  showCamera: boolean = false;
  showConfirmed: boolean = false;
  qrResultString: string;
  locationSub: Subscription;
  location: location;
  showNotConfirmed: boolean = false;
  messageError: string = '';
  @ViewChild('scanner', { static: false }) scanner: ZXingScannerComponent;
  availableDevices: any[] = [];
  currentDevice: MediaDeviceInfo = null;
  qrBusy: boolean = false;

  //Texts
  QrScan = {en: 'QR Scan', es: 'Escanear QR', fr: 'Balayer QR'};
  DescText = {en: 'Please Scan the code to access to the check in. If your camera don\'t work please check manually', es: 'Por favor escanee el código con la cámara. En caso de no poder haga el checkeo manual', fr: 'Veuillez scanner le code pour accéder à l\'enregistrement. Si votre appareil photo ne fonctionne pas, veuillez vérifier manuellement'};
  ManualCheck = {en: 'Manual Check-in', es: 'Registro manual', fr: 'Enregistrement manuelle'};
  CheckError = {en: 'Check-in not confirmed', es: 'Registro no confirmado', fr: 'Inscription non confirmée'};
  CheckConfirmed = {en: 'Check-in confirmed', es: 'Registro confirmado', fr: 'Inscription confirmée'};

  constructor(
    private establishmentService: EstablishmentService,
    private blService: BusinessLogicService,
    public languageService: LanguageService
  ) {
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          this.location = res;
          this.showCamera = true;
        } else {
          //this.blService.createMessage('error','Enable GPS location');
        }
      }
    );
    this.blService.setCurrentLocation();
  }

  ngOnInit(): void {
    let devicesTemp = [];
    let counter = 1;
    navigator.mediaDevices
      .enumerateDevices()
      .then((devices) => {
        devices.forEach(function (device) {
          if (device.kind === 'videoinput') {
            devicesTemp.push({ device: device, name: 'camara ' + counter });
            counter++;
          }
        });
        this.availableDevices = devicesTemp;
        this.currentDevice = this.availableDevices[0].device;
      })
      .catch(function (err) {
        console.log(err.name + ': ' + err.message);
      });
  }

  onDeviceSelectChange(selectedValue: string) {
    this.currentDevice = this.availableDevices.filter(function (item) {
      return item.device.deviceId === selectedValue;
    })[0].device;
  }

  onCodeResult(resultString: string) {
    if (this.qrBusy) return;
    this.qrResultString = resultString;
    let today = new Date().getTime();
    this.qrBusy = true;
    this.establishmentService
      .postQRCheck(this.qrResultString, today, this.location)
      .pipe(
        finalize(() => {
          this.qrBusy = false;
        })
      )
      .subscribe(
        (response: any) => {
          if (response.success) {
            this.showConfirmed = true;
            this.showNotConfirmed = false;
            this.showCamera = false;
          } else {
            this.showNotConfirmed = true;
          }
        },
        (err: any) => {
          this.showNotConfirmed = true;
          this.messageError = err.error?.message || '';
        }
      );
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

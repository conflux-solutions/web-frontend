import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthService } from '../services/user-auth.service';
import { Subscription } from 'rxjs';
import { UserResultsService } from '../services/user-results.service';
import { LanguageService } from '../services/language.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, OnDestroy {
  private userSub: Subscription;
  private qrSub: Subscription;
  showMenu: boolean = false;
  showFloatingQR: boolean = true;
  opened: boolean = true;
  displayDivClose: boolean = true;

  //Texts
  Home = {en: 'Search', es: 'Buscar', fr: 'Chercher'};
  LocateMe = {en: 'Locate Me', es: 'Ubicación', fr: 'Ubication'};
  QrScan = {en: 'QR Scan', es: 'Escanear QR', fr: 'Balayer QR'};
  MyTurns = {en: 'My shifts', es: 'Mis turnos', fr: 'Mes shifts'};
  ChangePassword = {en: 'Change Password', es: 'Cambiar contraseña', fr: 'Changer mot de passe'};

  constructor(
    private router: Router,
    private userAuthService: UserAuthService,
    private userResultsService: UserResultsService,
    public languageService: LanguageService
  ) {}

  ngOnInit() {
    this.userSub = this.userAuthService.user.subscribe((user) => {
      if (!!user) {
        this.showMenu = true;
      }
    });
    this.qrSub = this.userResultsService.oppenedStores.subscribe((bool) => {
      this.enableFloatingQR(bool);
    });
    this.opened =
      (document.documentElement.clientWidth || window.screen.width) > 650
        ? true
        : false;
    this.userResultsService.opennedMenu.next(this.opened);
  }

  changeDrawer() {
    this.opened = !this.opened;
    this.userResultsService.opennedMenu.next(this.opened);
    if (
      this.opened &&
      (document.documentElement.clientWidth || window.screen.width) < 601
    ) {
      this.displayDivClose = false;
    }
  }

  closeDrawer() {
    if (
      this.opened &&
      (document.documentElement.clientWidth || window.screen.width) < 601
    ) {
      this.opened = false;
      this.userResultsService.opennedMenu.next(this.opened);
      this.displayDivClose = true;
    }
  }

  enableFloatingQR(bool) {
    this.showFloatingQR = bool === 'false' ? false : true;
  }

  logOut() {
    this.userAuthService.logout();
  }

  logIn() {
    this.router.navigateByUrl('/user-auth');
  }

  ngOnDestroy() {
    if (this.userSub) this.userSub.unsubscribe();
    if (this.qrSub) this.qrSub.unsubscribe();
  }
}

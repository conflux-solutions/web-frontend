import { AbstractControl } from '@angular/forms';

export function selectValidator(control: AbstractControl): { [key: string]: any } | null {
  const valid = control.value == 'Choose' ? false : true;
  return valid ? null : { invalidOption: { valid: false, value: control.value } };
}

import { AbstractControl } from '@angular/forms';

export function passwordValidator(control: AbstractControl): { [key: string]: any } | null {
  let retVal = true;
  let message = '';
  // if(!(/[a-z]/.test(control.value))) {
  //   retVal = false;
  //   message = 'Password do not have lower-case letters';
  // }
  // if(!(/[A-Z]/.test(control.value))) {
  //   retVal = false;
  //   message = 'Password do not have upper-case letters';
  // }
  // if(!(/\d/.test(control.value))) {
  //   retVal = false;
  //   message = 'Password do not have any number';
  // }
  return retVal ? null : { invalidOption: { valid: false, value: control.value, message: message } };
}

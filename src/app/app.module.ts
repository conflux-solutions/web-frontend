import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  registerLocaleData,
  HashLocationStrategy,
  PathLocationStrategy,
  LocationStrategy,
  APP_BASE_HREF,
} from '@angular/common';
import en from '@angular/common/locales/en';
import { OwnerAuthComponent } from './auth/owner-auth/owner-auth.component';
import { OwnerRegisterComponent } from './auth/owner-register/owner-register.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AuthExternComponent } from './auth/auth-extern/auth-extern.component'; 
import { UserRegisterComponent } from './auth/user-register/user-register.component';
import { UserAuthComponent } from './auth/user-auth/user-auth.component';
import { GlobalErrorHandler } from './error/global-error-handler';
import { ServerErrorInterceptor } from './error/server-error.interceptor';
import { HomeComponent } from '../home/home.component';
import { UpdatePasswordComponent } from '../update-password/update-password.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    OwnerAuthComponent,
    OwnerRegisterComponent,
    ForgotComponent,
    AuthExternComponent,
    UserAuthComponent,
    UserRegisterComponent,
    HomeComponent,
    UpdatePasswordComponent,
  ],
  imports: [
    BrowserModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatMenuModule,
    MatDividerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule
  ],
  providers: [
    Location, {provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: APP_BASE_HREF, useValue: '/' },
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    { provide: HTTP_INTERCEPTORS, useClass: ServerErrorInterceptor, multi: true } 
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnerAuthComponent } from './auth/owner-auth/owner-auth.component';
import { OwnerRegisterComponent } from './auth/owner-register/owner-register.component';
import { OwnerAuthGuard } from './auth/owner-auth.guard';
import { AuthExternComponent } from './auth/auth-extern/auth-extern.component';
import { UserAuthComponent } from './auth/user-auth/user-auth.component';
import { UserRegisterComponent } from './auth/user-register/user-register.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import { HomeComponent } from '../home/home.component';
import { UpdatePasswordComponent } from '../update-password/update-password.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home/en' },
  { path: 'owner-auth', component: OwnerAuthComponent },
  { path: 'user-auth', component: UserAuthComponent },
  { path: 'owner-register', component: OwnerRegisterComponent },
  { path: 'user-register', component: UserRegisterComponent },
  { path: 'forgot/:role', component: ForgotComponent},
  { path: 'home/:language', component: HomeComponent},
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule) },
  { path: 'owner', loadChildren: () => import('./owner/owner.module').then(m => m.OwnerModule), canActivate:[OwnerAuthGuard] },
  { path: 'login/:success/:role/:token', component: AuthExternComponent },
  { path: 'update-password/:role/:token', component: UpdatePasswordComponent },
  { path: '**', redirectTo: '/home/en' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

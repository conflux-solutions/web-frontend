import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OwnerAuthService } from './services/owner-auth.service';
import { UserAuthService } from './services/user-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router, private ownerAuthService: OwnerAuthService, private userAuthSevice: UserAuthService) {
    this.ownerAuthService.autoLogin();
    this.userAuthSevice.autoLogin();
  }

  btnUser() {
    this.router.navigateByUrl('/user');
  }

  btnOwner() {
    this.router.navigateByUrl('/owner');
  }
}

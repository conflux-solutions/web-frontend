import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OwnerAuthService } from 'src/app/services/owner-auth.service';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { BusinessLogicService } from 'src/app/services/businessLogic.service';
import { finalize } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  isSpinning: boolean = false;
  role: string = 'user';

  //Texts
  RecoverPasswordText = {en: 'Recover Password', es: 'Recuperar contraseña', fr: 'Récupérer le mot de passe'};
  RecoverText = {en: 'Recover', es: 'Recuperar', fr: 'Récupérer'};
  WellcomeToConfluxText = {en: 'Welcome to Confflux', es: 'Bienvenido a Confflux', fr: 'Bienvenue à Confflux'};
  JoinConfluxText = {en: 'Join to Conflux and get instantaneous occupation status of establishments and crowds control', es: 'Únase a Conflux y obtenga el estado de ocupación instantánea de los establecimientos y el control de multitudes', fr: 'Rejoignez Conflux et obtenez le statut d\'occupation instantané des établissements et le contrôle des foules'};

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private ownerAuthService: OwnerAuthService,
    private userAuthService: UserAuthService,
    private blService: BusinessLogicService,
    private activateRoute: ActivatedRoute,
    public languageService: LanguageService
  ) {
    this.role = this.activateRoute.snapshot.params['role'];
  }

  validateForm: FormGroup;

  submitForm(): void {
    let email = this.validateForm.controls['email'].value;
    this.isSpinning = true;
    let service = this.role === 'owner' ? this.ownerAuthService : this.userAuthService;
    service.postChangePasswordMethod(email).pipe(
      finalize(() => {this.isSpinning = false;} )
    ).subscribe(
      (response: any) => {
        let route = this.role === 'owner' ? '/owner' : '/user';
          this.router.navigateByUrl(route);
          let message = {
            en: 'We have sent you an email to change your password',
            es: 'Le hemos enviado un correo para cambiar la contraseña',
            fr: 'Nous vous avons envoyé un e-mail pour changer votre mot de passe'
          };
          this.blService.createMessage('success', message);
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was a problem registering. Please check your connection',
          es: err.error.message.es || 'Hubo un problema al registrarse. Por favor verifique su conexión',
          fr: err.error.message.fr || 'Un problème est survenu lors de l\'enregistrement. Veuillez vérifier votre connexion'
        };
        this.blService.createMessage('error', message);
      }
    );
  }


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]]
    });
  }
}

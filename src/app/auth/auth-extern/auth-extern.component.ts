import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OwnerAuthService } from '../../services/owner-auth.service';
import { UserAuthService } from '../../services/user-auth.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-auth-extern',
  templateUrl: './auth-extern.component.html',
  styleUrls: ['./auth-extern.component.css']
})
export class AuthExternComponent implements OnInit {
  //Texts
  WellcomeToConfluxText = {en: 'Welcome to Confflux', es: 'Bienvenido a Confflux', fr: 'Bienvenue à Confflux'};
  JoinConfluxText = {en: 'Join to Conflux and get instantaneous occupation status of establishments and crowds control', es: 'Únase a Conflux y obtenga el estado de ocupación instantánea de los establecimientos y el control de multitudes', fr: 'Rejoignez Conflux et obtenez le statut d\'occupation instantané des établissements et le contrôle des foules'};

  constructor(private activeRoute: ActivatedRoute, private ownerAuthService: OwnerAuthService, private userAuthService: UserAuthService, private router: Router, private blService: BusinessLogicService, public languageService: LanguageService) { }

  ngOnInit(): void {
    let success = this.activeRoute.snapshot.params['success'];
    let urlAccess = this.activeRoute.snapshot.params['role'] === 'owner' ? '/owner' : '/user';
    let urlAccessError = this.activeRoute.snapshot.params['role'] === 'owner' ? '/owner-auth' : '/user-auth';
    if(success === 'success') {
      let token = this.activeRoute.snapshot.params['token'];
      if(this.activeRoute.snapshot.params['role'] === 'owner' ) {
        this.ownerAuthService.getSignInToken(token).subscribe(
          (response: any) => {
              this.router.navigateByUrl(urlAccess);
          },
          (err) => {
            let message = {
              en: err.error?.message.en || 'There was an error in the authentication, please try again.',
              es: err.error?.message.es || 'Hubo un error en la autenticación por favor intente de nuevo',
              fr: err.error?.message.fr || 'Une erreur s\'est produite lors de l\'authentification, veuillez réessayer.'
            };
            this.blService.createMessage('error', message);
            this.router.navigateByUrl(urlAccessError);
          }
        );
      } else {
        this.userAuthService.getSignInToken(token).subscribe(
          (response: any) => {
              this.router.navigateByUrl(urlAccess);
          },
          (err) => {
            let message = {
              en: err.error?.message.en || 'There was an error in the authentication, please try again.',
              es: err.error?.message.es || 'Hubo un error en la autenticación por favor intente de nuevo',
              fr: err.error?.message.fr || 'Une erreur s\'est produite lors de l\'authentification, veuillez réessayer.'
            };
            this.blService.createMessage('error', message);
            this.router.navigateByUrl(urlAccessError);
          }
        );
      }
    }
  }

}

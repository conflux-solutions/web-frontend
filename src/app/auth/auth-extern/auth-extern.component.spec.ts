import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthExternComponent } from './auth-extern.component';

describe('AuthExternComponent', () => {
  let component: AuthExternComponent;
  let fixture: ComponentFixture<AuthExternComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthExternComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthExternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

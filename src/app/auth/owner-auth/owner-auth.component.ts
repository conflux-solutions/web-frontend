import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OwnerAuthService } from 'src/app/services/owner-auth.service';
import { BusinessLogicService } from 'src/app/services/businessLogic.service';
import { finalize } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-owner-auth',
  templateUrl: './owner-auth.component.html',
  styleUrls: ['./owner-auth.component.css'],
})
export class OwnerAuthComponent implements OnInit {
  isSpinningLogin: boolean = false;
  validateForm: FormGroup;
  checked: boolean = true;

   //Texts
   OwnerLogin = {en: 'Owner Log In', es: 'Autenticación Propietario', fr: 'Connexion propriétaire'};
   EmailText = {en: 'EMAIL', es: 'CORREO ELECTRÓNICO', fr: 'EMAIL'};
   PasswordText = {en: 'PASSWORD', es: 'CONTRASEÑA', fr: 'MOT DE PASSE'};
   ForgotPasswordText = {en: 'Forgot password', es: 'Olvido contraseña', fr: 'Mot de passe oublié'};
   RemembermeText = {en: 'Remember me', es: 'Recordarme', fr: 'Rappeler'};
   SigninText = {en: 'Sign in', es: 'Entrar', fr: 'Se connecter'};
   NotAccountText = {en: 'Don\'t have an account?', es: 'No tengo cuenta', fr: 'Je n\'ai pas de compte'};
   RegisterText = {en: 'Register', es: 'Registrarse', fr: 'S\'inscrire'};
   WellcomeToConfluxText = {en: 'Welcome to Confflux', es: 'Bienvenido a Confflux', fr: 'Bienvenue à Confflux'};
   JoinConfluxText = {en: 'Join to Conflux and get instantaneous occupation status of establishments and crowds control', es: 'Únase a Conflux y obtenga el estado de ocupación instantánea de los establecimientos y el control de multitudes', fr: 'Rejoignez Conflux et obtenez le statut d\'occupation instantané des établissements et le contrôle des foules'};
   socialWayText = {en: 'or via Social Media', es: 'o por Redes Sociales', fr: 'ou par les Réseaux Sociaux'};

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private ownerAuthService: OwnerAuthService,
    private blService: BusinessLogicService,
    public languageService: LanguageService
  ) {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }

  ngOnInit(): void {
  }

  submitForm(): void {
    let email = this.validateForm.controls['email'].value;
    let password = this.validateForm.controls['password'].value;
    let remember = this.validateForm.controls['remember'].value;
    this.isSpinningLogin = true;
    this.ownerAuthService.postSignIn(email, password, remember).pipe(
      finalize(() => {this.isSpinningLogin = false;} )
    ).subscribe(
      (response: any) => {
          this.router.navigateByUrl('/owner');
      },
      err => {
        let message = {
          en: err.error.message.en || 'The email or the password are wrong',
          es: err.error.message.es || 'El correo electrónico o la contraseña son incorrectos',
          fr: err.error.message.fr || 'L\'e-mail ou le mot de passe sont incorrects'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  AuthGoogle() {
    window.location.href = 'https://shrouded-mountain-38177.herokuapp.com/auth/google?type=owner';
  }

  AuthFacebook() {
    window.location.href = 'https://shrouded-mountain-38177.herokuapp.com/auth/facebook?type=owner';
  }
}

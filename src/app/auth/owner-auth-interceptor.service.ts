import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpHeaders
} from '@angular/common/http';
import { take, exhaustMap } from 'rxjs/operators';
import { OwnerAuthService } from '../services/owner-auth.service';

@Injectable()
export class OwnerAuthInterceptorService implements HttpInterceptor {
  constructor(private ownerAuthService: OwnerAuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.ownerAuthService.user.pipe(
      take(1),
      exhaustMap(user => {
        if (!user) {
          return next.handle(req);
        }
        const authReq = req.clone({
          headers: req.headers.append('Authorization', user.token)
        });
        return next.handle(authReq);
      })
    );
  }
}

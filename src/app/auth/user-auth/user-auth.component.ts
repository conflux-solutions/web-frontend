import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserAuthService } from '../../services/user-auth.service';
import { finalize } from 'rxjs/operators';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.css'],
})
export class UserAuthComponent implements OnInit {
  isSpinningLogin: boolean = false;
  checked: boolean = true;
  validateForm: FormGroup;
  storeId: string;

  //Texts
  UserLogin = {en: 'User Log In', es: 'Autenticación Usuario', fr: 'Connexion d\'utilisateur'};
  EmailText = {en: 'EMAIL', es: 'CORREO ELECTRÓNICO', fr: 'EMAIL'};
  PasswordText = {en: 'PASSWORD', es: 'CONTRASEÑA', fr: 'MOT DE PASSE'};
  ForgotPasswordText = {en: 'Forgot password', es: 'Olvido contraseña', fr: 'Mot de passe oublié'};
  RemembermeText = {en: 'Remember me', es: 'Recordarme', fr: 'Rappeler'};
  SigninText = {en: 'Sign in', es: 'Entrar', fr: 'Se connecter'};
  NotAccountText = {en: 'Don\'t have an account?', es: 'No tengo cuenta', fr: 'Je n\'ai pas de compte'};
  RegisterText = {en: 'Register', es: 'Registrarse', fr: 'S\'inscrire'};
  WellcomeToConfluxText = {en: 'Welcome to Confflux', es: 'Bienvenido a Confflux', fr: 'Bienvenue à Confflux'};
  JoinConfluxText = {en: 'Join to Conflux and get instantaneous occupation status of establishments and crowds control', es: 'Únase a Conflux y obtenga el estado de ocupación instantánea de los establecimientos y el control de multitudes', fr: 'Rejoignez Conflux et obtenez le statut d\'occupation instantané des établissements et le contrôle des foules'};
  socialWayText = {en: 'or via Social Media', es: 'o por Redes Sociales', fr: 'ou par les Réseaux Sociaux'};

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userAuthService: UserAuthService,
    private activeRoute: ActivatedRoute,
    private blService: BusinessLogicService,
    public languageService: LanguageService
  ) {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }

  ngOnInit(): void {
    this.storeId = this.activeRoute.snapshot.queryParamMap.get('id') || null;
  }

  submitForm(): void {
    let email = this.validateForm.controls['email'].value;
    let password = this.validateForm.controls['password'].value;
    let remember = this.validateForm.controls['remember'].value;
    this.isSpinningLogin = true;
    this.userAuthService.postSignIn(email, password, remember).pipe(
      finalize(() => {this.isSpinningLogin = false;} )
    ).subscribe(
      (response: any) => {
        if(this.storeId) {
          this.router.navigateByUrl('/user/user-store/'+this.storeId);
        } else {
          this.router.navigateByUrl('/user');
        }
      },
      err => {
        let message = {
          en: err.error.message.en || 'The email or the password are wrong',
          es: err.error.message.es || 'El correo electrónico o la contraseña son incorrectos',
          fr: err.error.message.fr || 'L\'e-mail ou le mot de passe sont incorrects'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  AuthGoogle() {
    window.location.href = 'https://shrouded-mountain-38177.herokuapp.com/auth/google?type=user';
  }

  AuthFacebook() {
    window.location.href = 'https://shrouded-mountain-38177.herokuapp.com/auth/facebook?type=user';
  }
}

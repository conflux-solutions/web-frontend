import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpHeaders
} from '@angular/common/http';
import { take, exhaustMap } from 'rxjs/operators';
import { UserAuthService } from '../services/user-auth.service';

@Injectable()
export class UserAuthInterceptorService implements HttpInterceptor {
  constructor(private userAuthService: UserAuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.userAuthService.user.pipe(
      take(1),
      exhaustMap(user => {
        if (!user) {
          return next.handle(req);
        }
        const authReq = req.clone({
          headers: req.headers.append('Authorization', user.token)
        });
        return next.handle(authReq);
      })
    );
  }
}

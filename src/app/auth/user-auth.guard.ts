import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserAuthService } from '../services/user-auth.service';
import { map, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class UserAuthGuard implements CanActivate {
 
    constructor(private userAuthService: UserAuthService, private router: Router) {
    }
 
    canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> {

    return this.userAuthService.user.pipe(
        map(user => {
            return !!user;
        }),
        tap(isAuth => {
            if(!isAuth) {
                this.router.navigate(['/user-auth']);
            }
        })
    );
 }
}
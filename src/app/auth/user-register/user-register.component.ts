import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { passwordValidator } from 'src/app/validators/password.validator';
import { Router } from '@angular/router';
import { UserAuthService } from '../../services/user-auth.service';
import { finalize } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
})
export class UserRegisterComponent implements OnInit {
  validateForm: FormGroup;
  isSpinningRegister: boolean = false;
  messagePassword: string = '';
  showMessage;

    //Texts
    UserRegisterText = {en: 'User registration', es: 'Registro de Usuario', fr: 'Enregistrement d\'utilisateur'};
    EmailText = {en: 'EMAIL', es: 'CORREO ELECTRÓNICO', fr: 'EMAIL'};
    ConfirmEmailText = {en: 'We have sent an email to ', es: 'Hemos enviado un correo a ', fr: 'Nous avons envoyé un email à '};
    PasswordText = {en: 'PASSWORD', es: 'CONTRASEÑA', fr: 'MOT DE PASSE'};
    RegisterText = {en: 'Register', es: 'Registrarse', fr: 'Enregistrement'};
    OrText = {en: 'Or', es: 'O', fr: 'Ou'};
    IHaveText = {en: 'I have an account', es: 'Ya tengo cuenta', fr: 'J\'ai un compte'};
    WellcomeToConfluxText = {en: 'Welcome to Confflux', es: 'Bienvenido a Confflux', fr: 'Bienvenue à Confflux'};
    JoinConfluxText = {en: 'Join to Conflux and get instantaneous occupation status of establishments and crowds control', es: 'Únase a Conflux y obtenga el estado de ocupación instantánea de los establecimientos y el control de multitudes', fr: 'Rejoignez Conflux et obtenez le statut d\'occupation instantané des établissements et le contrôle des foules'};
    
  constructor(
    private fb: FormBuilder,
    private blService: BusinessLogicService,
    private userAuthService: UserAuthService,
    private router: Router,
    public languageService: LanguageService
  ) {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8), passwordValidator]],
    });
  }

  ngOnInit(): void {
  }

  submitForm() {
    let email = this.validateForm.controls['email'].value;
    let password = this.validateForm.controls['password'].value;
    this.showMessage = {
      show: true,
      message: this.ConfirmEmailText[this.languageService.language] + email
    }
    this.isSpinningRegister = true;
    this.userAuthService.postSignUp(email, password).pipe(
      finalize(() => {
        this.isSpinningRegister = false;
      } )
    ).subscribe(
      (response: any) => {
        let message = {
          en: 'Successful, We have sent you an email',
          es: 'Exitoso, te hemos enviado un correo electrónico',
          fr: 'Succès, nous vous avons envoyé un e-mail'
        };
        this.blService.createMessage('success', message);
        this.router.navigateByUrl('/user');
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was a problem registering. Please check your connection',
          es: err.error.message.es || 'Hubo un problema al registrarse. Por favor verifique su conexión',
          fr: err.error.message.fr || 'Il y a eu un problème d\'enregistrement. Veuillez vérifier votre connexion'
        };
        this.blService.createMessage('error', message);
      }
    )
  }
}

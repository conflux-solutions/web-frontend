import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OwnerAuthService } from '../services/owner-auth.service';
import { map, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class OwnerAuthGuard implements CanActivate {
 
    constructor(private ownerAuthService: OwnerAuthService, private router: Router) {
    }
 
    canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> {

    return this.ownerAuthService.user.pipe(
        map(user => {
            return !!user;
        }),
        tap(isAuth => {
            if(!isAuth) {
                this.router.navigate(['/owner-auth']);
            }
        })
    );
 }
}
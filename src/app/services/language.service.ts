import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LanguageService {
  language: string = 'en';
  allowedLanguages = ['en','es','fr'];
  
  constructor() {
    let userLang = navigator.language || 'en-En';
    userLang = userLang.split('-')[0];
    userLang = this.allowedLanguages.includes(userLang) ? userLang : 'en';
    this.language = userLang;
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { owner, store, OwnerData, location } from '../models/types';
import { BehaviorSubject } from 'rxjs';
import { BusinessLogicService } from './businessLogic.service';

@Injectable()
export class OwnerService {
  owner = new BehaviorSubject<any>(null);

  constructor(
    private http: HttpClient,
    private envService: EnvService,
    private blService: BusinessLogicService
  ) {}

  BACKEND_URL = this.envService.host.backend.backend;

  postQrCode(id: string) {
    return this.http.post(this.BACKEND_URL + 'owners/qr-code', { id_establishment: id });
  }

  getStores() {
    this.http.get(this.BACKEND_URL + 'owners').subscribe(
      (resData: any) => {
        if (resData.success) {
          this.owner.next(resData);
        } else {
          let message = {
            en: 'The email or the password are wrong',
            es: 'El correo electrónico o la contraseña son incorrectos',
            fr: 'L\'e-mail ou le mot de passe sont incorrects'
          };
          this.blService.createMessage('error', message);
        }
      },
      (err) => {
        let message = {
          en: 'The email or the password are wrong',
          es: 'El correo electrónico o la contraseña son incorrectos',
          fr: 'L\'e-mail ou le mot de passe sont incorrects'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  putPassword(password: string) {
    return this.http.put(this.BACKEND_URL + 'owners/password', {
      password: password,
    });
  }

  getStore(id: string) {
    return this.http.get(
      this.BACKEND_URL + 'establishment/get-by-id?id=' + id
    );
  }

  getShifts(id: string) {
    return this.http.get(
      this.BACKEND_URL + 'shifts?id_establishment=' + id
    );
  }
}

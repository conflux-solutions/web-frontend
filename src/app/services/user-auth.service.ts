import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { OwnerUser } from '../models/types';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './env.service';
import { tap, catchError } from 'rxjs/operators';
import { BusinessLogicService } from './businessLogic.service';

@Injectable({ providedIn: 'root' })
export class UserAuthService {
  user = new BehaviorSubject<OwnerUser>(null);
  private tokenExpirationTimer: any;

  constructor(
    private router: Router,
    private http: HttpClient,
    private envService: EnvService,
    private blService: BusinessLogicService
  ) {}

  BACKEND_URL = this.envService.host.backend.backend;

  postChangePasswordMethod(email: string) {
    return this.http.post(this.BACKEND_URL + 'users/forgot', {email: email});
  }

  postSigninGoogle(role: string){
    let queryParams = `?type=${role}`;
    return this.http.get(this.BACKEND_URL + 'auth/google' + queryParams);
  }

  changeForgottenPassword(password: string, token: string) {
    return this.http.put(this.BACKEND_URL + 'users/update-password', 
      {
        password: password,
        token: token
      }
    );
  }

  getSignInToken(token: string) {
    var header = {
      headers: new HttpHeaders().set('Authorization',  `Bearer ${token}`)
    }
    return this.http.get(this.BACKEND_URL + 'users',  header).pipe(
      catchError(error=>throwError(error)),
      tap((resData: {success: boolean, id_user: string, token: string, expiration: number}) => {
          this.handleAuthentication(
            resData.id_user,
            resData.token,
            resData.expiration,
            true
          );
      }));
  }
  
  postSignIn(email: string, password: string, remember: boolean) {
    return this.http.post(this.BACKEND_URL + 'users/signin', {email: email, password: password}).pipe(
      catchError(error=>throwError(error)),
      tap((resData: {success: boolean, id_user: string, token: string, expiration: number}) => {
          this.handleAuthentication(
            resData.id_user,
            resData.token,
            resData.expiration,
            remember
          );
      }));
  }

  postSignUp(email: string, password: string) {
    return this.http.post(this.BACKEND_URL + 'users/signup', {email: email, password: password}).pipe(
      catchError(error=>throwError(error)),
      tap((resData: {success: boolean, id_user: string, token: string, expiration: number}) => {
          this.handleAuthentication(
            resData.id_user,
            resData.token,
            resData.expiration,
            false
          );
      }));
  }

  autoLogin() {
    const userData: {
      id_user: string;
      _token: string;
      _tokenExpirationDate: string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }

    const loadedUser = new OwnerUser(
      userData.id_user,
      userData._token,
      new Date(userData._tokenExpirationDate)
    );

    if (loadedUser.token) {
      this.user.next(loadedUser);
      const expirationDuration =
        new Date(userData._tokenExpirationDate).getTime() -
        new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  logout() {
    this.user.next(null);
    this.router.navigateByUrl('/user-auth', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/user']);
    }); 
    localStorage.removeItem('userData');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
  }

  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  private handleAuthentication(
    userId: string,
    token: string,
    expiresIn: number,
    remember: boolean
  ) {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const user = new OwnerUser(userId, token, expirationDate);
    this.user.next(user);
    this.autoLogout(expiresIn * 1000);
    if(remember) {
      localStorage.setItem('userData', JSON.stringify(user));
    }
  }
}

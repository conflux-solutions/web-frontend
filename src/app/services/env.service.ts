import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js
  public envFileLoaded = false

  public host = { //dummy values
    backend: {
      backend: 'https://shrouded-mountain-38177.herokuapp.com/',
      googleAutocomplete: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
      googleMaps: 'https://maps.googleapis.com/maps/api/place/details/json',
      OSMSearchByString: 'https://api.openstreetmap.org/api/0.6/notes/search.json',
      OSMAutocomplete: 'https://nominatim.openstreetmap.org/search',
      OSMRevcomplete: 'https://nominatim.openstreetmap.org/reverse',
    },
    googlekey: environment.googleKey
  };

  constructor() {
  }
}

import { Injectable } from '@angular/core';
import { location } from '../models/types';
import { BehaviorSubject } from 'rxjs';
import { GoogleMapService } from './googlemaps.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LanguageService } from './language.service';

@Injectable({ providedIn: 'root' })
export class BusinessLogicService {
  //variables
  set_radius: string[] = ['500m', '1 Km', '2 Kms', '4 Kms', '8 Kms'];
  zoom_values = { '500': 16, '1000': 15, '2000': 14, '4000': 13, '8000': 12 };
  local_location = new BehaviorSubject<location>(null);
  local_display_name = {
    country: 'France',
    country_code: 'FR',
    city: 'Paris',
    address: '',
  };
  default_latitude = '48.856613';
  default_longitude = '2.352222';
  busy: boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    private googleService: GoogleMapService,
    private languageService: LanguageService
  ) {}

  getDeviceLocation() {
    if (this.busy) return;
    let options = {
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0,
    };
    if (navigator.geolocation) {
      this.busy = true;
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.busy = false;
          const getLocation: location = {
            latitude: position.coords.latitude.toString(),
            longitude: position.coords.longitude.toString(),
          };
          this.googleService.getAdminLevelByLocation(getLocation).subscribe(
            (response: any) => {
              if (response.place_id) {
                this.local_display_name = {
                  country: response.address.country,
                  country_code: response.address.country_code,
                  city: this.getCity(response.address),
                  address: response.display_name,
                };
              }
              this.local_location.next(getLocation);
            },
            (err) => {
              this.local_location.next(getLocation);
            }
          );
        },
        (err) => {
          this.busy = false;
          let message;
          switch (err.code) {
            case 1:
              message = {
                en: 'You disabled GPS',
                es: 'Tiene el GPS desactivado',
                fr: 'Vous avez désactivé le GPS'
              };
              break;
            case 3:
              message = {
                en: 'Your device GPS takes longer than expected',
                es: 'El dispositivo GPS tarda más de lo esperado',
                fr: 'Le GPS de votre appareil prend plus de temps que prévu'
              };
              break;
            default:
              message = {
                en: 'Your device dont allow authomatic location. Check permissions',
                es: 'Su dispositivo no permite la ubicación automática. Verificar permisos',
                fr: 'Votre appareil ne permet pas la localisation automatique. Vérifier les autorisations'
              };
              break;
          }
          this.createMessage('error', message);
          this.local_location.next({
            latitude: this.default_latitude,
            longitude: this.default_longitude,
          });
        },
        options
      );
    } else {
      this.local_location.next({
        latitude: this.default_latitude,
        longitude: this.default_longitude,
      });
      let message = {
        en: 'Your device dont allow authomatic location. Check permissions',
        es: 'Su dispositivo no permite la ubicación automática. Verificar permisos',
        fr: 'Votre appareil ne permet pas la localisation automatique. Vérifier les autorisations'
      };
      this.createMessage(
        'error',
        message
      );
    }
  }

  setCurrentLocation() {
    if (!this.local_location.getValue()) {
      this.getDeviceLocation();
    }
  }

  setManualLocation(location: location) {
    this.local_location.next(location);
    let message = {
      en: 'The location was set',
      es: 'La localización se ha establecido',
      fr: 'La localisation a été défini'
    };
    this.createMessage('success', message);
  }

  getCity(address: any) {
    if (address.state) {
      return address.state;
    }
    if (address.county) {
      return address.county;
    }
    if (address.city) {
      return address.city;
    }
    if (address.town) {
      return address.town;
    }
    if (address.village) {
      return address.village;
    }
    return '';
  }

  createMessage(
    type: string,
    text: { en: string; es: string; fr: string }
  ): void {
    this.snackBar.open(`${text[this.languageService.language]}`, '', {
      duration: 2000,
    });
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { location } from '../models/types';

@Injectable({ providedIn: 'root' })
export class ParamsService {
  
  constructor(private http: HttpClient,
    private envService:EnvService        
  ) {}
  
  BACKEND_URL = this.envService.host.backend.backend;

  getShopsParams() {
    return this.http.get(this.BACKEND_URL + 'confluences/params');
  }

  geCountryParams() {
    return this.http.get(this.BACKEND_URL + 'confluences/countries');
  }
}
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { location } from '../models/types'

@Injectable({ providedIn: 'root' })
export class GoogleMapService {
  
  constructor(private http: HttpClient,
    private envService:EnvService        
    ) {}
    
  GOOGLE_AUTOCOMPLETE_BACKEND_URL = this.envService.host.backend.googleAutocomplete;
  GOOGLE_MAPS_BACKEND_URL = this.envService.host.backend.googleAutocomplete;
  KEY = this.envService.host.googlekey;
  OPENSTREETMAPS_BACKEND_URL = this.envService.host.backend.OSMSearchByString;
  OPENSTREETMAPS_AUTO_BACKEND_URL = this.envService.host.backend.OSMAutocomplete;
  OPENSTREETMAPS_REVERSE_BACKEND_URL = this.envService.host.backend.OSMRevcomplete;

  getAutocomplete(text: string) {
    let query = text.replace(' ','+');
    return this.http.get( this.OPENSTREETMAPS_AUTO_BACKEND_URL + query);
  }

  getAutocompleteNominatim(text: string, city: string, country: string) {
    let query = text.replace(' ','+').replace('#','+');
    let queryParams = `?street=${query}&format=json&county=${city}&city=${city}&country=${country}&limit=4`;
    return this.http.get(this.OPENSTREETMAPS_AUTO_BACKEND_URL + queryParams);
  }

  getAdminLevelByLocation(location: location) {
    let queryParams = `?format=json&lat=${location.latitude}&lon=${location.longitude}&zoom=10&addressdetails=1`; 
    return this.http.get(this.OPENSTREETMAPS_REVERSE_BACKEND_URL + queryParams);
  }

  getLocationByString(text){
    return this.http.get(this.OPENSTREETMAPS_BACKEND_URL + `?q=${text}&limit=1`);
  }

  getLocation(place_id: string) {
    let queryParamas = `?placeid=${place_id}&key=${this.KEY}`;
    return this.http.get( this.GOOGLE_MAPS_BACKEND_URL + queryParamas);
  }
}
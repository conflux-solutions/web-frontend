import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { location, Establishment } from '../models/types';

@Injectable()
export class EstablishmentService {
  pipe(arg0: import("rxjs").MonoTypeOperatorFunction<unknown>) {
    throw new Error("Method not implemented.");
  }
  constructor(private http: HttpClient, private envService: EnvService) { }

  BACKEND_URL = this.envService.host.backend.backend;
  BACKEND_URL_ESTABLISHTMENT = `${this.BACKEND_URL}establishment`;

  searchEstablishmentById(id: string, category: string, location: location) {
    return this.http.post(
      this.BACKEND_URL_ESTABLISHTMENT + `/search-by-id?id=${id}`,
      { category, location }
    );
  }

  getEstablishment(
    location: location,
    radius: string,
    category: string,
    offset: number,
    limit: number
  ) {
    let queryParams = `?lat=${location.latitude}&lng=${location.longitude}&radius=${radius}&category=${category}`;
    let offsetParams = `&offset=${offset}&limit=${limit}`;
    return this.http.get(
      this.BACKEND_URL_ESTABLISHTMENT + queryParams + offsetParams
    );
  }

  getEstablishmentLocationsByCategory(category: string) {
    return this.http.get(
      this.BACKEND_URL_ESTABLISHTMENT + +`/location?category=${category}`
    );
  }

  getEstablishmentCategories() {
    return this.http.get(this.BACKEND_URL_ESTABLISHTMENT + +`/category`);
  }

  postQRCheck(qr_code: string, date: number, location: location) {
    return this.http.post(
      this.BACKEND_URL_ESTABLISHTMENT + '/checkin',
      {
        qr_code: qr_code,
        date: date,
        location: {latitude: location.latitude, longitude: location.longitude}
      }
    )
  }

  addEstablishment(establishment: Establishment) {
    return this.http.post(
      this.BACKEND_URL_ESTABLISHTMENT + '/new',
      establishment
    );
  }

  changeStatusEstablishment(id:string, status:boolean) {
    let queryParams = `?id=${id}`;
    return this.http.put(
      this.BACKEND_URL_ESTABLISHTMENT + '/update-state' + queryParams,
      {isActive: status}
    );
  }

  putEstablishment(establishment: Establishment) {
    return this.http.put(
      this.BACKEND_URL_ESTABLISHTMENT + `/update?id=${establishment.id}`,
      establishment
    );
  }

  deleteEstablishment(id: string) {
    return this.http.delete(
      this.BACKEND_URL_ESTABLISHTMENT + `/remove?id=${id}`
    );
  }
}

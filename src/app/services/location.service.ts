import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { location } from '../models/types';

@Injectable()
export class LocationService {
  constructor(private http: HttpClient, private envService: EnvService) {}

  BACKEND_URL = this.envService.host.backend.backend;
  BACKEND_URL_ESTABLISHTMENT = `${this.BACKEND_URL}establishment`;

  createNewLocation(location: location) {
    const {
      longitude,
      latitude,
      city,
      stateCode,
      postalCode,
      countryCode,
      street,
      type,
    } = location;

    return this.http.post(this.BACKEND_URL_ESTABLISHTMENT + `/new-location`, {
      longitude,
      latitude,
      city,
      stateCode,
      postalCode,
      countryCode,
      street,
      type,
    });
  }
}

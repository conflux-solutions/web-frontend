import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserResultsService {
  opennedMenu = new BehaviorSubject<boolean>(null);
  oppenedStores = new BehaviorSubject<string>(null);
  
  constructor() {}

  private results: any[] = [];
  private searchText: string = '';

  setResults(obj) {
    this.results = obj;
  }

  getResults() {
    return this.results;
  }

  setsearchText(obj) {
    this.searchText = obj;
  }

  getsearchText() {
    return this.searchText;
  }
}

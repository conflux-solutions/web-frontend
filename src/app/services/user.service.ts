import { HttpClient } from '@angular/common/http';
import { Injectable, Query } from '@angular/core';
import { EnvService } from './env.service';
import { location } from '../models/types';

@Injectable()
export class UserService {
  constructor(private http: HttpClient, private envService: EnvService) {}

  BACKEND_URL = this.envService.host.backend.backend;

  getStoresByLocByName(location: any, title: string, radius: string) {
    let queryParams = `?lat=${location.latitude}&lng=${location.longitude}&radius=${radius}&category=${title}`;
    return this.http.get(this.BACKEND_URL + 'establishment' + queryParams);
  }

  searchEstablishment(query: string, location: any, radius: string) {
    let queryParams = `?query=${query}&lat=${location.latitude}&lng=${location.longitude}&radius=${radius}`;
    return this.http.get(
      this.BACKEND_URL + 'establishment/search' + queryParams
    );
  }

  getShifts(id: string, date: number) {
    let queryParams = `?id_establishment=${id}&date=${date}`;
    return this.http.get(this.BACKEND_URL + 'shifts/get-by-date' + queryParams);
  }

  postShift(date: number, observation: string, id: string) {
    return this.http.post(this.BACKEND_URL + 'shifts/new', {
      shift_date: date,
      comments: observation,
      id_establishment: id,
    });
  }

  putPassword(password: string) {
    return this.http.put(this.BACKEND_URL + 'users/password', {
      password: password,
    });
  }

  getStore(id: string) {
    let queryParams = `?id=${id}`;
    return this.http.get(
      this.BACKEND_URL + 'establishment/get-by-id' + queryParams
    );
  }

  getShift(id: string) {
    let queryParams = `?id=${id}`;
    return this.http.get(this.BACKEND_URL + 'shifts/get-by-id' + queryParams);
  }

  putShift(date: number, observation: string, id: string) {
    let queryParams = `?id=${id}`;
    return this.http.put(this.BACKEND_URL + 'shifts/update' + queryParams, {
      shift_date: date,
      comments: observation,
      id_shift: id,
    });
  }

  deleteShift(id: string) {
    let queryParams = `?id=${id}`;
    return this.http.delete(this.BACKEND_URL + 'shifts/remove' + queryParams);
  }

  getShiftsUser() {
    return this.http.get(this.BACKEND_URL + 'shifts/get-by-userid');
  }

  checkManual(code: string, location: location, date: number) {
    return this.http.post(this.BACKEND_URL + 'establishment/checkin-code', {
      shift_code: code,
      location: { latitude: location.latitude, longitude: location.longitude },
      date: date
    });
  }
}

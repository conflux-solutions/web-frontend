import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';
import {
  getParams,
  location,
  place,
  Establishment,
} from 'src/app/models/types';
import { BusinessLogicService } from 'src/app/services/businessLogic.service';
import { ParamsService } from 'src/app/services/params.service';
import { GoogleMapService } from 'src/app/services/googlemaps.service';
import { selectValidator } from 'src/app/validators/select.validator';
import { debounceTime, finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { OwnerService } from 'src/app/services/owner.service';
import { EstablishmentService } from 'src/app/services/establishment.service';
import { LanguageService } from '../../services/language.service';
import { FirebaseStorageService } from '../../services/firebase-storage.service';

declare var ol: any;

@Component({
  selector: 'app-edit-store',
  templateUrl: './edit-store.component.html',
  styleUrls: ['./edit-store.component.css'],
})
export class EditStoreComponent implements OnInit, OnDestroy {
  zoom = 15;
  validateForm: FormGroup;
  location: location = null;
  isSpinningRegister: boolean = false;
  isSpinning: boolean = false;
  isSpinningMap: boolean = true;
  category: string;
  categories: any[] = [];
  storeId: string = null;
  storeName: string = '';
  model;
  countryInput: Subject<string> = new Subject<string>();
  cityInput: Subject<string> = new Subject<string>();
  addressInput: Subject<string> = new Subject<string>();
  allCountries: place[] = [];
  countries: place[] = [];
  country: place = null;
  addresses: any[] = [];
  locationSub: Subscription;
  isSpinningCountry: boolean = true;
  isSpinningCity: boolean = false;
  protectedAddress: boolean = false;
  addressShow: boolean = true;
  isSpinningCategories: boolean = false;
  isSpinningSave: boolean = false;
  isSpinningLoad: boolean = true;
  globalLatitude: number = parseFloat(this.blService.default_latitude);
  globalLongitude: number = parseFloat(this.blService.default_longitude);
  map: any;
  graphicLayer: any;
  locationTimeout;
  messageFile;
  formDataFile = new FormData();
  porcentaje = 0;
  srcImageFile;
  nameFile: string = null;
  saveFileFunc = new BehaviorSubject<string>(null);
  slots = [
    { number: 30, label: '30 min' },
    { number: 60, label: '1 hour' },
    { number: 90, label: '1:30 hour' },
    { number: 120, label: '2 hours' },
    { number: 150, label: '2:30 hours' },
    { number: 180, label: '3 hours' },
  ];
  hours = [
    { number: 0, label: '12 AM' },
    { number: 1, label: '1 AM' },
    { number: 2, label: '2 AM' },
    { number: 3, label: '3 AM' },
    { number: 4, label: '4 AM' },
    { number: 5, label: '5 AM' },
    { number: 6, label: '6 AM' },
    { number: 7, label: '7 AM' },
    { number: 8, label: '8 AM' },
    { number: 9, label: '9 AM' },
    { number: 10, label: '10 AM' },
    { number: 11, label: '11 AM' },
    { number: 12, label: '12 PM' },
    { number: 13, label: '1 PM' },
    { number: 14, label: '2 PM' },
    { number: 15, label: '3 PM' },
    { number: 16, label: '4 PM' },
    { number: 17, label: '5 PM' },
    { number: 18, label: '6 PM' },
    { number: 19, label: '7 PM' },
    { number: 20, label: '8 PM' },
    { number: 21, label: '9 PM' },
    { number: 22, label: '10 PM' },
    { number: 23, label: '11 PM' },
  ];

  //Texts
  EditStoreText = {
    en: 'Edit store',
    es: 'Editar Establecimiento',
    fr: 'Éditer établissement',
  };
  RegisterStoreText = {
    en: 'REGISTER STORE',
    es: 'REGISTRAR ESTABLECIMIENTO',
    fr: 'INSCRIPTION ÉTABLISSEMENT',
  };
  NameText = { en: 'Name', es: 'Nombre', fr: 'Nom' };
  PhoneText = { en: 'Phone', es: 'Teléfono', fr: 'téléphone' };
  ChooseCategoryText = {
    en: 'Choose Category',
    es: 'Escoger categoría',
    fr: 'Choisissez une catégorie',
  };
  DescriptionText = {
    en: 'Description of the store',
    es: 'Descripción del establecimiento',
    fr: 'Description de l\'établissement',
  };
  MaxAffluenceText = {
    en: 'Maximum affluency',
    es: 'Máxima afluencia permitida',
    fr: 'Affluence maximale',
  };
  ServiceTimeText = {
    en: 'Shift duration',
    es: 'Duración de turnos',
    fr: 'Durée du shift',
  };
  WorkingDaysText = {
    en: 'Oppening days',
    es: 'Días abiertos',
    fr: "Jours d'ouverture",
  };
  DaysText = [
    { en: 'Sunday', es: 'Domingo', fr: 'Dimanche' },
    { en: 'Monday', es: 'Lunes', fr: 'Lundi' },
    { en: 'Tuesday', es: 'Martes', fr: 'Mardi' },
    { en: 'Wednsday', es: 'Miércoles', fr: 'Mercredi' },
    { en: 'Thursday', es: 'Jueves', fr: 'Jeudi' },
    { en: 'Friday', es: 'Viernes', fr: 'Vendredi' },
    { en: 'Saturday', es: 'Sábado', fr: 'Samedi' },
  ];
  StartHourText = {
    en: 'Start hour',
    es: 'Hora de inicio',
    fr: 'Heure de début',
  };
  EndHourText = {
    en: 'End hour',
    es: 'Hora de cierre',
    fr: 'Heure de fermeture',
  };
  CountryText = { en: 'Country', es: 'País', fr: 'Pays' };
  CityText = { en: 'City', es: 'Ciudad', fr: 'Ville' };
  StreetText = { en: 'District', es: 'Zona', fr: 'District' };
  OptionsText = { en: 'options', es: 'opciones', fr: "d'options" };
  MoreText = { en: 'More', es: 'Más', fr: 'Plus' };
  LessText = { en: 'Less', es: 'Menos', fr: 'Moins' };
  SetLocationText = {
    en: 'Set this location',
    es: 'Establecer esta ubicación',
    fr: 'Définir cet emplacement',
  };
  PleaseCheckText = {
    en: 'Verify that your establishment is well located on the map. Detail your location by tapping on the map',
    es:
      'Verifique que su establecimiento esté bien ubicado en el mapa. Detalle su ubicación tocando en el mapa',
    fr:
      'Vérifiez que votre établissement est bien situé sur la carte. Détaillez votre emplacement en appuyant sur la carte',
  };
  GetDeviceText = {
    en: 'Get device location',
    es: 'Ubicación del dispositivo',
    fr: "Emplacement de l'appareil",
  };
  ShownAddressText = {
    en: 'Shown address',
    es: 'Dirección para mostrar',
    fr: 'Adresse à afficher',
  };
  SaveText = { en: 'Save', es: 'Guardar', fr: 'Sauvegarder' };
  selectFileText = {
    en: 'Select a file for the logo (png, jpg)',
    es: 'Seleccionar un archivo para el logo (png, jpg)',
    fr: 'Sélectionnez un fichier pour le logo (png, jpg)',
  };
  nofileText = {
    en: 'There is no file',
    es: 'No hay archivo',
    fr: "Il n'y a pas de fichier",
  };
  uploadText = {
    en: 'Upload File',
    es: 'Subir Archivo',
    fr: 'Télécharger fichier',
  };
  featuresText = {
    en: 'Features',
    es: 'Caracteristicas',
    fr: 'Caractéristiques',
  };
  allowShiftsText = {
    en: 'Enable shift use',
    es: 'Habilitar uso de turnos',
    fr: 'Activer l\'utilisation des shifts',
  };

  constructor(
    private fb: FormBuilder,
    private blService: BusinessLogicService,
    private paramsService: ParamsService,
    private googleService: GoogleMapService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private ownerService: OwnerService,
    private establishmentService: EstablishmentService,
    public languageService: LanguageService,
    private firebaseStorage: FirebaseStorageService
  ) {
    this.storeId = this.activeRoute.snapshot.params['id'];
    this.validateForm = this.fb.group({
      country: [null],
      city: [null],
      address: [null],
      name: [null, [Validators.required]],
      storeCategory: ['Choose', [selectValidator]],
      phone: [null, Validators.required],
      addressText: [null, Validators.required],
      people: [null, Validators.required],
      slot_time: [null, Validators.required],
      start_hour: [null, Validators.required],
      end_hour: [null, Validators.required],
      daySunday: [false],
      dayMonday: [false],
      dayTuesday: [false],
      dayWendsday: [false],
      dayThursday: [false],
      dayFriday: [false],
      daySaturday: [false],
      fileLogo: [null],
      enableShifting: [true],
      description: [null],
    });
  }

  ngOnInit(): void {
    this.map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM(),
        }),
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([this.globalLongitude, this.globalLatitude]),
        zoom: 17,
      }),
    });
    this.mapClicked();
    this.locationSub = this.blService.local_location.subscribe(
      (res: location) => {
        if (res) {
          this.location = res;
          let countryName = this.blService.local_display_name.country;
          let code = this.blService.local_display_name.country_code;
          let cityName = this.blService.local_display_name.city;
          let addressName = this.blService.local_display_name.address;
          this.protectedAddress = true;
          this.validateForm.patchValue({
            country: countryName,
            city: cityName,
            address: addressName,
          });
          this.country = { name: countryName, code: code };
          this.globalLatitude = parseFloat(res.latitude);
          this.globalLongitude = parseFloat(res.longitude);
          this.setCenterMap({
            latitude: this.globalLatitude,
            longitude: this.globalLongitude,
          });
          let message = {
            en: 'Located',
            es: 'Localizado',
            fr: 'Localisé',
          };
          this.blService.createMessage('success', message);
          clearTimeout(this.locationTimeout);
          this.isSpinningMap = false;
        }
      }
    );
    if (this.storeId === 'new') {
      this.locationTimeout = setTimeout(() => {
        this.isSpinningMap = false;
        let message = {
          en:
            'Location is taking longer than expected. Check if GPS is enabled',
          es:
            'La Localización está tardando más de lo esperado. Compruebe si el GPS está habilitado',
          fr:
            'La localization prend plus de temps que prévu. Vérifiez si le GPS est activé',
        };
        this.blService.createMessage('error', message);
      }, 5000);
      this.blService.setCurrentLocation();
      this.isSpinningLoad = false;
    }
    this.isSpinningMap = false;
    if (this.storeId) {
      this.isSpinning = true;
      this.paramsService.getShopsParams().subscribe(
        (response: getParams) => {
          let params = response.params;
          this.categories = params.filter(function (item) {
            return item.name === 'Categories';
          })[0].value;
          this.isSpinning = false;
          this.paramsService.geCountryParams().subscribe(
            (response: any) => {
              if (response.success) {
                this.allCountries = response.countries;
                this.isSpinningCountry = false;
              }
            },
            (err) => {
              let message = {
                en: 'There was an error loading countries',
                es: 'Hubo un error al cargar los países',
                fr: "Une erreur s'est produite lors du chargement des pays",
              };
              this.blService.createMessage('error', message);
              this.isSpinningCountry = false;
            }
          );
          if (this.storeId === 'new') {
            this.storeName = 'New';
            this.initKeyUpListenerAddress();
            return;
          }
          this.ownerService.getStore(this.storeId).subscribe(
            (response: any) => {
              if (response.success) {
                this.isSpinningLoad = false;
                this.srcImageFile =
                  response.establishment?.establishment_pics_url[1];
                if(this.srcImageFile) {
                  this.nameFile = response.establishment?.establishment_pics_url[0];
                  // document.body.removeChild(link);
                  // var xhr = new XMLHttpRequest();
                  // xhr.open("GET", this.srcImageFile, true);
                  // xhr.responseType = "blob";
                  // xhr.onload = function(){
                  //     var urlCreator = window.URL || window.webkitURL;
                  //     var imageUrl = urlCreator.createObjectURL(this.response);
                  //     var tag = document.createElement('a');
                  //     tag.href = imageUrl;
                  //     tag.download = 'hola.png';
                  //     document.body.appendChild(tag);
                  //     tag.click();
                  //     document.body.removeChild(tag);
                  // }
                  // xhr.send();
                }
                this.storeName = response.establishment.name;
                this.location = {
                  latitude: response.establishment.location.latitude,
                  longitude: response.establishment.location.longitude,
                };
                if (this.map) {
                  this.globalLatitude = parseFloat(this.location.latitude);
                  this.globalLongitude = parseFloat(this.location.longitude);
                  this.setCenterMap({
                    latitude: this.globalLatitude,
                    longitude: this.globalLongitude,
                  });
                }
                let start_hour = new Date(
                  response.establishment.opening_hours.open_hour || null
                ).getUTCHours();
                let end_hour = new Date(
                  response.establishment.opening_hours.close_hour || null
                ).getUTCHours();
                this.validateForm.patchValue({
                  name: response.establishment.name,
                  storeCategory: response.establishment.category,
                  country: response.establishment.location.country || null,
                  city: response.establishment.location.city,
                  address: response.establishment.location.address || null,
                  phone: response.establishment.phone || null,
                  addressText:
                    response.establishment.location.stateCode || null,
                  people: response.establishment.max_affluences_allowed || null,
                  slot_time: response.establishment.slot_size || null,
                  start_hour: start_hour,
                  end_hour: end_hour,
                  description: response.establishment.description,
                  daySunday: response.establishment.opening_hours.day.includes(
                    0
                  )
                    ? true
                    : false,
                  dayMonday: response.establishment.opening_hours.day.includes(
                    1
                  )
                    ? true
                    : false,
                  dayTuesday: response.establishment.opening_hours.day.includes(
                    2
                  )
                    ? true
                    : false,
                  dayWendsday: response.establishment.opening_hours.day.includes(
                    3
                  )
                    ? true
                    : false,
                  dayThursday: response.establishment.opening_hours.day.includes(
                    4
                  )
                    ? true
                    : false,
                  dayFriday: response.establishment.opening_hours.day.includes(
                    5
                  )
                    ? true
                    : false,
                  daySaturday: response.establishment.opening_hours.day.includes(
                    6
                  )
                    ? true
                    : false,
                  enableShifting: response.establishment?.enableShifting || false
                });
                this.country = {
                  name: response.establishment.location.country,
                  code: response.establishment.location.countryCode,
                };
                this.initKeyUpListenerAddress();
              } else {
                let message = {
                  en:
                    'It seems that you dont have permission to look at it. Please reload your authentication',
                  es:
                    'Parece que no tiene permiso para ingresar. Vuelva a cargar su autenticación',
                  fr:
                    "Il semble que vous n'ayez pas la permission de le regarder. Veuillez recharger votre authentification",
                };
                this.blService.createMessage('error', message);
                this.router.navigate(['/owner/manage-stores']);
              }
            },
            (err) => {
              let message = {
                en: 'There was an error loading the store',
                es: 'Hubo un error al cargar el establecimiento',
                fr: "Une erreur s'est produite lors du chargement du magasin",
              };
              this.blService.createMessage('error', message);
              this.router.navigate(['/owner/manage-stores']);
            }
          );
        },
        (err) => {
          let message = {
            en: 'There was an error loading parameters',
            es: 'Hubo un error al cargar los parámetros',
            fr: "Une erreur s'est produite lors du chargement des paramètres",
          };
          this.blService.createMessage('error', message);
          this.router.navigate(['/owner/manage-stores']);
          this.isSpinning = false;
        }
      );
    }

    this.saveFileFunc.subscribe((res: string) => {
      if (!res) return;
      this.isSpinningSave = true;
      let city = this.validateForm.controls['city'].value.trim();
      let country = this.validateForm.controls['country'].value.trim();
      let address = this.validateForm.controls['address'].value.trim();
      let addressText = this.validateForm.controls['addressText'].value.trim();
      let phone = this.validateForm.controls['phone'].value.toString().trim();
      let people = this.validateForm.controls['people'].value;
      let slot_time = this.validateForm.controls['slot_time'].value;
      let enableShifting = this.validateForm.controls['enableShifting'].value;
      let description = this.validateForm.controls['description'].value;
      let open_hour = new Date().setUTCHours(
        this.validateForm.controls['start_hour'].value
      );
      let close_hour = new Date().setUTCHours(
        this.validateForm.controls['end_hour'].value
      );
      let code = this.allCountries.filter(function (item) {
        return item.name.toLowerCase() === country.toLowerCase();
      })[0].code;
      let image = res === ' ' ? this.srcImageFile : res;
      let category_name: string = this.validateForm.controls['storeCategory']
        .value;
      let name = this.validateForm.controls['name'].value;
      let day = [];
      if (this.validateForm.controls['daySunday'].value) {
        day.push(0);
      }
      if (this.validateForm.controls['dayMonday'].value) {
        day.push(1);
      }
      if (this.validateForm.controls['dayTuesday'].value) {
        day.push(2);
      }
      if (this.validateForm.controls['dayWendsday'].value) {
        day.push(3);
      }
      if (this.validateForm.controls['dayThursday'].value) {
        day.push(4);
      }
      if (this.validateForm.controls['dayFriday'].value) {
        day.push(5);
      }
      if (this.validateForm.controls['daySaturday'].value) {
        day.push(6);
      }
      let storeLocation: location = {
        latitude: this.location.latitude,
        longitude: this.location.longitude,
        city: city,
        postalCode: '',
        countryCode: code,
        country: country,
        street: '',
        type: '',
        display_name: address,
        address: address,
        stateCode: addressText,
      };

      const establishment: Establishment = {
        id: this.storeId,
        location: storeLocation,
        name: name,
        category_name: category_name,
        opening_hours: {
          day: day,
          open_hour: open_hour,
          close_hour: close_hour,
        },
        phone: phone,
        max_affluences_allowed: people,
        shift_schedule_max_hours: 50,
        slot_size: slot_time,
        enableShifting: enableShifting,
        establishment_pics_url: [this.nameFile, image],
        description: description
      };

      if (this.storeId === 'new') {
        this.model = this.establishmentService.addEstablishment(establishment);
      } else {
        this.model = this.establishmentService.putEstablishment(establishment);
      }
      this.model.subscribe(
        (res: any) => {
          this.ownerService.getStores();
          let messageNew = {
            en: 'Store created',
            es: 'Establecimiento creado',
            fr: 'Établissement creé',
          };
          let messageUpdate = {
            en: 'Store updated',
            es: 'Establecimiento actualizado',
            fr: 'Établissement actualice',
          };
          this.blService.createMessage(
            'success',
            this.storeId === 'new' ? messageNew : messageUpdate
          );
          this.router.navigate(['/owner/manage-stores']);
          this.isSpinningSave = false;
        },
        (err) => {
          let message = {
            en: 'Error updating store',
            es: 'Error al actualizar la tienda',
            fr: 'Erreur lors de la mise à jour du magasin',
          };
          this.blService.createMessage('error', message);
          this.isSpinningSave = false;
        }
      );
    });
  }

  initKeyUpListenerAddress() {
    this.countryInput.pipe(debounceTime(700)).subscribe((data: string) => {
      this.countries = [];
      let completed = 4;
      let last_get = 0;
      let compare = this.validateForm.controls['country'].value
        .trim()
        .toLowerCase();
      for (let i = 0; i < this.allCountries.length; i++) {
        if (this.allCountries[i].name.toLowerCase().includes(compare)) {
          this.countries.push(this.allCountries[i]);
          last_get = i;
          completed -= 1;
        }
        if (completed <= 0) break;
      }
      if (this.protectedAddress) {
        this.protectedAddress = false;
        return;
      }
      if (completed >= 3) {
        this.validateForm.patchValue({ city: null, address: null });
        if (!(compare === this.allCountries[last_get].name.toLowerCase())) {
          this.validateForm.controls['country'].setErrors({ incorrect: true });
        }
      }
    });

    this.addressInput.pipe(debounceTime(700)).subscribe((data: string) => {
      let address = this.validateForm.controls['address'].value?.trim();
      let city = this.validateForm.controls['city'].value?.trim();
      let country = this.validateForm.controls['country'].value.trim();
      if (address && country) {
        this.googleService
          .getAutocompleteNominatim(address, city, country)
          .subscribe((response: any) => {
            this.addresses = [];
            this.addresses.push(...response);
          });
      }
    });
  }

  setFirstAddress() {
    this.validateForm.patchValue({
      address: this.addresses[0]?.display_name,
    });
  }

  public greaterThan(subj: number, num: number) {
    return subj <= num;
  }

  resetHour() {
    if (
      this.validateForm.controls.start_hour.value >
      this.validateForm.controls.end_hour.value
    ) {
      this.validateForm.patchValue({
        end_hour: this.validateForm.controls.start_hour.value,
      });
    }
  }

  mapClicked() {
    this.map.on('click', (args) => {
      var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      let location = { latitude: lonlat[1], longitude: lonlat[0] };
      this.updateTextLocation(location);
      this.setCenterMap(location);
    });
  }

  updateTextLocation(location) {
    this.location = location;
    this.googleService
      .getAdminLevelByLocation(this.location)
      .subscribe((response: any) => {
        if (response.place_id) {
          let countryName = response.address.country;
          let code = response.address.country_code;
          let cityName = this.blService.getCity(response.address);
          let addressName = response.display_name;
          this.protectedAddress = true;
          this.validateForm.patchValue({
            country: countryName,
            city: cityName,
            address: addressName,
          });
          this.country = { name: countryName, code: code };
        }
      });
  }

  setCenterMap(loc) {
    var view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([loc.longitude, loc.latitude]));
    view.setZoom(17);
    if (this.graphicLayer) {
      this.map.removeLayer(this.graphicLayer);
    }
    this.graphicLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [
          new ol.Feature({
            geometry: new ol.geom.Point(
              ol.proj.transform(
                [loc.longitude, loc.latitude],
                'EPSG:4326',
                'EPSG:3857'
              )
            ),
          }),
        ],
      }),
      style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: 'assets/icons/fullLocation.svg',
        }),
      }),
    });
    this.map.addLayer(this.graphicLayer);
  }

  setPlaceID(addressID) {
    if (!addressID || addressID === 'undefined') {
      return;
    }
    let el = this.addresses.filter(function (item) {
      return item.place_id === addressID;
    })[0];
    this.globalLatitude = parseFloat(el.lat);
    this.globalLongitude = parseFloat(el.lon);
    this.setCenterMap({
      latitude: this.globalLatitude,
      longitude: this.globalLongitude,
    });
    this.location = { latitude: el.lat, longitude: el.lon };
  }

  setPlaceStringID() {
    let compare = this.validateForm.controls['address'].value;
    let el = this.addresses.filter(function (item) {
      return item.display_name === compare;
    })[0];
    this.setPlaceID(el.place_id);
  }

  setPlaceString() {
    let address = this.validateForm.controls['address'].value.trim();
    let city = this.validateForm.controls['city'].value.trim();
    let country = this.validateForm.controls['country'].value.trim();
    this.googleService
      .getAutocompleteNominatim(address, city, country)
      .subscribe((response: any) => {
        if (response.length > 0) {
          let el = response[0];
          this.globalLatitude = parseFloat(el.lat);
          this.globalLongitude = parseFloat(el.lon);
          this.setCenterMap({
            latitude: this.globalLatitude,
            longitude: this.globalLongitude,
          });
          this.location = {
            latitude: el.lat,
            longitude: el.lon,
          };
        }
      });
  }

  changed(value) {
    switch (value) {
      case 'country':
        this.countryInput.next();
        break;
      case 'city':
        this.cityInput.next();
        break;
      case 'address':
        this.addressInput.next();
        break;
      default:
        break;
    }
  }

  submitForm() {
    let archivo = this.formDataFile.get('fileLogo');
    if (!this.nameFile) {
      this.nameFile =
        new Date().getTime().toString() +
        this.location.latitude.toString().substring(0, 6) +
        this.location.longitude.toString().substring(0, 6) +
        Math.floor(Math.random() * 1000).toString();
    }
    this.isSpinningSave = true;
    if (archivo) {
      let extFile = '.' + archivo['name'].split('.').pop().toLowerCase();
      let referencia = this.firebaseStorage.referenciaCloudStorage(this.nameFile + extFile);
      let tarea = this.firebaseStorage.tareaCloudStorage(this.nameFile + extFile, archivo);
      tarea
        .percentageChanges()
        .pipe(
          finalize(() => {
            referencia.getDownloadURL().subscribe((URL) => {
              this.saveFileFunc.next(URL);
            });
          })
        )
        .subscribe((porcentaje) => {
          this.porcentaje = Math.round(porcentaje);
        });
    } else {
      this.saveFileFunc.next(' ');
    }
  }

  getManualLocation() {
    this.locationTimeout = setTimeout(() => {
      this.isSpinningMap = false;
      let message = {
        en: 'Location is taking longer than expected. Check if GPS is enabled',
        es:
          'La Localización está tardando más de lo esperado. Compruebe si el GPS está habilitado',
        fr:
          'La localization prend plus de temps que prévu. Vérifiez si le GPS est activé',
      };
      this.blService.createMessage('error', message);
    }, 5000);
    this.isSpinningMap = true;
    this.blService.getDeviceLocation();
  }

  returnManager() {
    this.router.navigate(['/owner/manage-stores']);
  }

  public fileChanged(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        let ext = event.target.files[i].name.split('.').pop().toLowerCase() || null;
        if(ext !== 'jpg' && ext !== 'png' && ext !== 'jpeg') {
          let message = {
            en: 'Wrong file format',
            es: 'Formato de archivo incorrecto',
            fr: 'Format de fichier incorrect',
          };
          this.blService.createMessage('error',message);
          continue;
        }
        this.messageFile = `${event.target.files[i].name}`;
        this.formDataFile.delete('fileLogo');
        this.formDataFile.append(
          'fileLogo',
          event.target.files[i],
          event.target.files[i].name
        );
        let reader = new FileReader();
        reader.onloadend = (e) => {
          this.srcImageFile = reader.result;
        };
        reader.readAsDataURL(event.target.files[i]);
      }
    } else {
      this.messageFile = this.nofileText[this.languageService.language];
    }
  }

  ngOnDestroy() {
    if (this.locationSub) this.locationSub.unsubscribe();
  }
}

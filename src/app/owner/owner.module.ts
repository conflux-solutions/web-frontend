import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwnerComponent } from './owner.component';
import { QrCodeComponent } from './qr-code/qr-code.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatGridListModule} from '@angular/material/grid-list'; 
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete'; 
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { QRCodeModule } from 'angularx-qrcode';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { ManageStoresComponent } from './manage-stores/manage-stores.component';
import { EditStoreComponent } from './edit-store/edit-store.component';
import { OwnerService } from '../services/owner.service';
import { OwnerAuthInterceptorService } from '../auth/owner-auth-interceptor.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EstablishmentService } from '../services/establishment.service';
import { ReviewStoreComponent } from './review-store/review-store.component';
import { OwnerDialogComponent } from './owner-dialog/owner-dialog.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../../environments/environment';

const routes: Routes = [
  {
    path: '',
    component: OwnerComponent,
    children: [
      { path: '', component: ManageStoresComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'qr-code', component: QrCodeComponent },
      { path: 'manage-stores', component: ManageStoresComponent },
      { path: 'edit-store/:id', component: EditStoreComponent },
      { path: 'review-store/:id', component: ReviewStoreComponent },
      { path: '**', component: ManageStoresComponent },
    ],
  },
];

@NgModule({
  declarations: [
    ProfileComponent,
    OwnerComponent,
    QrCodeComponent,
    ManageStoresComponent,
    EditStoreComponent,
    ReviewStoreComponent,
    OwnerDialogComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatIconModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatTableModule,
    FormsModule,
    QRCodeModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule
  ],
  providers: [
    OwnerService,
    EstablishmentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: OwnerAuthInterceptorService,
      multi: true,
    },
  ],
})
export class OwnerModule {}

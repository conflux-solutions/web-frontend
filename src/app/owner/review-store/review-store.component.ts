import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OwnerService } from '../../services/owner.service';
import { BusinessLogicService } from '../../services/businessLogic.service';
import { MatPaginator } from '@angular/material/paginator';
import { finalize, debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LanguageService } from '../../services/language.service';

const elementsPerPage = 10;

@Component({
  selector: 'app-review-store',
  templateUrl: './review-store.component.html',
  styleUrls: ['./review-store.component.css']
})
export class ReviewStoreComponent implements OnInit {
  category: string;
  categories: string[] = [];
  storeId: string = null;
  storeName: string = '';
  displayedColumns: string[] = ['user','hour','date','comments'];
  dataSet;
  dataSetGlobal;
  dataSource;
  isSpinningShifts: boolean = true;
  total: number = 0;
  pages: number[] = [];
  offset = 0;
  currentPage = 0;
  manualPage = 1;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  pageSize = elementsPerPage;
  searchInput: Subject<string> = new Subject<string>();
  searchText: string;

  //Texts
  StoreText = {en: 'Store', es: 'Establecimiento', fr: 'Établissement'};
  ShiftsText = {en: 'Shifts', es: 'Turnos', fr: 'Shifts'};
  SearchShiftsText = {en: 'Search one shift', es: 'Busca un turno', fr: 'Rechercher un shift'};
  DateText = {en: 'DATE', es: 'FECHA', fr: 'DATE'};
  ObservationsText = {en: 'OBSERVATIONS', es: 'OBSERVACIONES', fr: 'OBSERVATIONS'};
  HourText = {en: 'HOUR', es: 'HORA', fr: 'HEURE'};
  CodeText = {en: 'CODE', es: 'CÓDIGO', fr: 'CODE'};

  constructor(
    private activeRoute: ActivatedRoute,
    private ownerService: OwnerService,
    private router: Router,
    private blService: BusinessLogicService,
    public languageService: LanguageService
  ) {
    this.storeId = this.activeRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    if (this.storeId) {
      this.ownerService.getShifts(this.storeId).pipe(
        finalize(() => {this.isSpinningShifts = false;} )
      ).subscribe(
        (response: any) => {
          if(response.success){
            this.storeName = response.establishment.name;
            this.dataSetGlobal = response.shifts;
            this.dataSet = this.dataSetGlobal;
            this.total = this.dataSet.length;
            this.dataSource = this.dataSet.slice(0, elementsPerPage);
          } else {
            let message = {
              en: 'There was an error loading shifts',
              es: 'Hubo un error al cargar los turnos',
              fr: 'Une erreur s\'est produite lors du chargement des shifts'
            };
            this.blService.createMessage('error', message);
          }
        },
        err => {
          let message = {
            en: err.error.message.en || 'There was an error loading shifts',
            es: err.error.message.es || 'Hubo un error al cargar los turnos',
            fr: err.error.message.fr || 'Une erreur s\'est produite lors du chargement des shifts'
          };
          this.blService.createMessage('error', message);
        },
      );
    }
    this.initKeyUpListenerAddress();
  }

  initKeyUpListenerAddress() {
    this.searchInput.pipe(debounceTime(700)).subscribe((data: string) => {
      this.dataSet = this.dataSetGlobal;
      if(this.searchText.trim() === '') {
        this.total = this.dataSet.length;
        this.resetPaginator();
        return;
      }
      let searchText = this.searchText.toLowerCase();
      this.dataSet = this.dataSet.filter(function(item) {
        return item.shift_code.toLowerCase().includes(searchText)
      });
      this.total = this.dataSet.length;
      this.resetPaginator();
    });
  }

  changed() {
    this.searchInput.next();
  }

  returnManager() {
    this.router.navigate(['/owner/manage-stores']);
  }

  resetPaginator() {
    this.manualPage = 1;
    this.offset = 0;
    if (this.paginator) {
        this.paginator.pageIndex = 0;
    }
    this.goToPage();
  }

  // Change paginations values when selected by user
  onChangePage(ev) {
    this.currentPage = ev.pageIndex;
    this.offset = elementsPerPage * this.currentPage;
    this.manualPage = this.currentPage + 1;
    this.goToPage();
  }

  // Change pagination page manually
  updateManualPage(ev) {
    this.currentPage = ev - 1;
    this.offset = elementsPerPage * this.currentPage;
    this.paginator.pageIndex = ev - 1;
    this.goToPage();
  }

  goToPage() {
    this.dataSource = this.dataSet.slice(this.offset, elementsPerPage+this.offset);
  }

  checkDate(date) {
    return new Date(date).toLocaleString().split(' ')[0];
  }

  checkHour(hours) {
    if (!hours) return '';
    let hour = new Date(hours).toLocaleString('en-US').split(' ')
    return hour[1].split(':').slice(0,2).join(':') + ' ' + hour[2];
  }
}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { BusinessLogicService } from 'src/app/services/businessLogic.service';
import { OwnerService } from 'src/app/services/owner.service';
import { OwnerAuthService } from 'src/app/services/owner-auth.service';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.css'],
})
export class QrCodeComponent implements OnInit, OnDestroy {
  private ownerSub: Subscription;
  showCode: boolean = false;
  isSpinning: boolean = false;
  isSpinningStores: boolean = true;
  codeString: string = '';
  link;
  storeId: string = null;
  dataSet;
  addressesImages = {en: 'template-en.png', es: 'template-es.png', fr: 'template-fr.png'}
  addressImage: string = 'assets/images/';
  imageLogo = new Image();

  @ViewChild('qrcode') canvaContainer;
  @ViewChild('imgSource') ImageContainer;

  //Texts
  QrCodeText = {en: 'QR Codes', es: 'Códigos QR', fr: 'Codes QR'};
  ChooseStoreText = {en: 'Choose store', es: 'Escoger establecimiento', fr: 'Choisissez l\'établissement'};
  GenerateText = {en: 'Generate new QR Code', es: 'Generar nuevo código QR', fr: 'Générer un nouveau code QR'};
  DownloadText = {en: 'Print', es: 'Imprimir', fr: 'Imprimer'};
  WelcomeText = {en: 'Wellcome to', es: 'Bienvenido(a) a', fr: 'Bienvenue à'};
  AskScanText = {en: 'Please scan the code before entering', es: 'Por favor escanear el código antes de entrar', fr: 'Veuillez scanner le code avant d\'entrer'};

  constructor(
    private ownerService: OwnerService,
    private blService: BusinessLogicService,
    private activeRoute: ActivatedRoute,
    public languageService: LanguageService
  ) {
    this.storeId = this.activeRoute.snapshot.queryParamMap.get('id') || null;
  }

  ngOnInit(): void {
    this.ownerSub = this.ownerService.owner.subscribe(owner => {
      if(!!owner) {
        this.isSpinningStores = false;
        this.dataSet = owner.establishments??[];
        this.dataSet = this.dataSet.filter((item) => {
          return item.isActive === true;
        });
        if(!this.storeId) {
          this.storeId = this.dataSet[0]._id
        }
        let element = this.dataSet.filter((item) => {
          return item._id === this.storeId;
        })[0];
        this.imageLogo.crossOrigin = "Anonymous";  // This enables CORS
        this.imageLogo.src = element?.establishment_pics_url[1];
        if(element.qr_code) {
          this.codeString = element.qr_code;
          this.showCode = true;
        } else {
          this.showCode = false;
        }
      }
    });
  }

  changeStore() {
    if(this.storeId) {
      let id = this.storeId;
      let element = this.dataSet.filter(function(item) {
        return item._id === id;
      })[0];
      this.imageLogo.src = element?.establishment_pics_url[1];
      if(element.qr_code) {
        this.codeString = element.qr_code;
        this.showCode = true;
      } else {
        this.showCode = false;
      }
    }
  }

  genQrCode() {
    this.isSpinning = true;
    this.ownerService.postQrCode(this.storeId).pipe(
      finalize(() => {this.isSpinning = false;} )
    ).subscribe(
      (response: any) => {
        if(response.success){
          this.codeString = response.qr_token;
          this.showCode = true;
          let index = this.dataSet.findIndex(item => item._id === this.storeId );
          if (index !== -1){
            this.dataSet[index].qr_code = this.codeString;
          }
        }
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was an error and the QR can not be generated',
          es: err.error.message.es || 'Hubo un error y el QR no se pudo generar',
          fr: err.error.message.fr || 'Il y a eu une erreur et le QR ne peut pas être généré'
        };
        this.blService.createMessage('error', message);
        this.showCode = false;
      }
    );
  }

  DownloadQrCode() {
    let id = this.storeId;
    let element = this.dataSet.filter(function(item) {
      return item._id === id;
    })[0];
    let canvas = this.canvaContainer.qrcElement.nativeElement.children[0];
    let canvas2 = this.ImageContainer.nativeElement;
    let dynamicCanvas = document.createElement("canvas");
    let dynamicContext = dynamicCanvas.getContext("2d");
    let dynamicCanvas2 = document.createElement("canvas");
    let dynamicContext2 = dynamicCanvas2.getContext("2d");
    let relationImageLogo = this.imageLogo.height/this.imageLogo.width;
    dynamicContext2.drawImage(this.imageLogo, 0, 0, this.imageLogo.width, this.imageLogo.height, 0, 0 , 200, 200*relationImageLogo);
    dynamicCanvas.height = canvas2.height;
    dynamicCanvas.width = canvas2.width;
    dynamicContext.fillStyle = "blue";
    dynamicContext.fillRect(0, 0, canvas2.width, canvas2.height);
    dynamicContext.drawImage(canvas2, 0, 0);
    dynamicContext.drawImage(dynamicCanvas2, 90, 300);
    dynamicContext.fillStyle = "black";
    dynamicContext.font = "40px Arial black";
    dynamicContext.fillText(element.name, 130, 103);
    dynamicContext.drawImage(canvas,420,590);
    let canvasDataUrl = dynamicCanvas.toDataURL()
      .replace(/^data:image\/[^;]*/, 'data:application/octet-stream');
    this.link = document.createElement('a');
    this.link.setAttribute('href', canvasDataUrl);
    this.link.setAttribute('target', '_blank');
    this.link.setAttribute('download', 'qr_code.jpg');
    this.link.click();
  }

  ngOnDestroy() {
    this.ownerSub.unsubscribe();
  }
}

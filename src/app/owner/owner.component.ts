import { Component, OnInit } from '@angular/core';
import { OwnerAuthService } from '../services/owner-auth.service';
import { OwnerService } from '../services/owner.service';
import { LanguageService } from '../services/language.service';

@Component({
  selector: 'app-owner',
  templateUrl: './owner.component.html',
  styleUrls: ['./owner.component.css'],
})
export class OwnerComponent implements OnInit {
  isCollapsed: boolean = false;
  opened: boolean = false;
  displayDivClose: boolean = true;

  //Texts
  ManageStoresText = {en: 'Manage stores', es: 'Administrar Establecimientos', fr: 'Gérer les établissements'};
  QrCode = {en: 'QR Codes', es: 'Códigos QR', fr: 'Codes QR'};
  ChangePassword = {en: 'Change Password', es: 'Cambiar contraseña', fr: 'Changer mot de passe'};

  constructor(
    private ownerAuthService: OwnerAuthService,
    private ownerService: OwnerService,
    public languageService: LanguageService
  ) {}

  ngOnInit() {
    this.opened =
      (document.documentElement.clientWidth || window.screen.width) > 600
        ? true
        : false;
    this.ownerService.getStores();
  }

  changeDrawer() {
    this.opened = !this.opened;
    if (
      this.opened &&
      (document.documentElement.clientWidth || window.screen.width) < 601
    ) {
      this.displayDivClose = false;
    }
  }

  closeDrawer() {
    if (
      this.opened &&
      (document.documentElement.clientWidth || window.screen.width) < 601
    ) {
      this.opened = false;
      this.displayDivClose = true;
    }
  }

  logOut() {
    this.ownerAuthService.logout();
  }
}

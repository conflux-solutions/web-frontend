import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { OwnerService } from 'src/app/services/owner.service';
import { BusinessLogicService } from 'src/app/services/businessLogic.service';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { EstablishmentService } from 'src/app/services/establishment.service';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OwnerDialogComponent } from '../owner-dialog/owner-dialog.component';
import { LanguageService } from '../../services/language.service';

const elementsPerPage = 10;

@Component({
  selector: 'app-manage-stores',
  templateUrl: './manage-stores.component.html',
  styleUrls: ['./manage-stores.component.css'],
})
export class ManageStoresComponent implements OnInit, OnDestroy {
  private ownerSub: Subscription;
  isSpinningStores: boolean = true;
  displayedColumns: string[] = ['name','review','edit','delete'];
  dataSet;
  dataSetGlobal;
  dataSource;
  total: number = 0;
  pages: number[] = [];
  offset = 0;
  currentPage = 0;
  emailOwner: string;
  manualPage = 1;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  pageSize = elementsPerPage;
  searchInput: Subject<string> = new Subject<string>();
  searchText: string;
  days = [
  {en: 'Su', es: 'Do', fr: 'Di'},
  {en: 'Mo', es: 'Lu', fr: 'Lu'},
  {en: 'Tu', es: 'Ma', fr: 'Ma'},
  {en: 'We', es: 'Mi', fr: 'Me'},
  {en: 'Th', es: 'Ju', fr: 'Je'},
  {en: 'Fr', es: 'Vi', fr: 'Ve'},
  {en: 'Sa', es: 'Sá', fr: 'Sa'}
  ];

  //Texts
  MyStoresText = {en: 'My Stores', es: 'Mis Establecimientos', fr: 'Mes établissements'};
  SearchText = {en: 'Search one of your stores', es: 'Busque uno de sus establecimientos', fr: 'Rechercher dans l\'un de vos magasins'};
  HelloText = {en: 'Hello', es: 'Hola', fr: 'Salut'};
  YouHaveText = {en: 'You have', es: 'Usted tiene', fr: 'Vous avez'};
  StoresText = {en: 'stores', es: 'establecimientos', fr: 'établissements'};
  EditText = {en: 'Edit', es: 'Editar', fr: 'Éditer'};
  DisableText = {en: 'Disbale', es: 'Deshabilitar', fr: 'Désactiver'};
  EnableText = {en: 'Enable', es: 'Habilitar', fr: 'Activer'};
  DeleteText = {en: 'Delete', es: 'Eliminar', fr: 'Supprimer'};
  NewStoreText = {en: 'New Store', es: 'Nuevo establecimiento', fr: 'Nouvele établissement'};
  AreSureEnableText = {en: 'Are you sure you want to enable this store?', es: '¿Está seguro de habilitar este establecimiento?', fr: 'Voulez-vous vraiment activer cet établissement?'};
  AreSureDisableText = {en: 'Are you sure you want to disable this store?', es: '¿Está seguro de deshabilitar este establecimiento?', fr: 'Voulez-vous vraiment désactiver cet établissement?'};
  AreSureDeleteText = {en: 'Are you sure you want to delete this store?', es: '¿Está seguro de eliminar este establecimiento?', fr: 'Voulez-vous vraiment supprimer cet établissement?'};
  TitleSureText = {en: 'Confirm', es: 'Confirmar', fr: 'Confirmer'};
  NoSureText = {en: 'No', es: 'No', fr: 'Non'};
  YesSureText = {en: 'Yes', es: 'Sí', fr: 'Oui'};
  AllDaysText = {en: 'All days', es: 'Todos los días', fr: 'Tous les jours'};
  DownloadText = {en: 'Print', es: 'Imprimir', fr: 'Imprimer'};

  constructor(
    private router: Router,
    private ownerService: OwnerService,
    private establishmentService: EstablishmentService,
    private blService: BusinessLogicService,
    public dialog: MatDialog,
    public languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.ownerSub = this.ownerService.owner.subscribe(owner => {
      if(!!owner) {
        this.dataSetGlobal = owner.establishments??[];
        this.dataSet = this.dataSetGlobal;
        this.total = this.dataSet.length;
        this.emailOwner = owner?.owner?.email;
        this.isSpinningStores = false;
        this.dataSource = this.dataSet.slice(0, elementsPerPage);
      } 
    });
    this.initKeyUpListenerAddress();
  }

  initKeyUpListenerAddress() {
    this.searchInput.pipe(debounceTime(700)).subscribe((data: string) => {
      this.dataSet = this.dataSetGlobal;
      if(this.searchText.trim() === '') {
        this.total = this.dataSet.length;
        this.resetPaginator();
        return;
      }
      let searchText = this.searchText.toLowerCase();
      this.dataSet = this.dataSet.filter(function(item) {
        return item.name.toLowerCase().includes(searchText)
      });
      this.total = this.dataSet.length;
      this.resetPaginator();
    });
  }

  changed() {
    this.searchInput.next();
  }

  openDialogDisableStore(id: string) {
    const dialogRef = this.dialog.open(OwnerDialogComponent, {
      data: {
        title: this.TitleSureText[this.languageService.language],
        data: this.AreSureDisableText[this.languageService.language],
        noText: this.NoSureText[this.languageService.language],
        yesText: this.YesSureText[this.languageService.language]
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==='yes') {
        this.disableStore(id);
      }
    });
  }

  openDialogEnableStore(id: string) {
    const dialogRef = this.dialog.open(OwnerDialogComponent, {
      data: {
        title: this.TitleSureText[this.languageService.language],
        data: this.AreSureEnableText[this.languageService.language],
        noText: this.NoSureText[this.languageService.language],
        yesText: this.YesSureText[this.languageService.language]
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==='yes') {
        this.enableStore(id);
      }
    });
  }

  openDialogDeleteStore(id: string) {
    const dialogRef = this.dialog.open(OwnerDialogComponent, {
      data: {
        title: this.TitleSureText[this.languageService.language],
        data: this.AreSureDeleteText[this.languageService.language],
        noText: this.NoSureText[this.languageService.language],
        yesText: this.YesSureText[this.languageService.language]
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result==='yes') {
        this.deleteStore(id);
      }
    });
  }

  editStore(id: string) {
    this.router.navigate(['/owner/edit-store/' + id]);
  }

  checkQR(id: string) {
    this.router.navigate(['owner/qr-code'], { queryParams: { id: id } }); 
  }

  reviewStore(establishment: any) {
    if(!establishment.isActive) return;
    this.router.navigate(['/owner/review-store/' + establishment._id]);
  }

  
  newStore() {
    this.router.navigate(['/owner/edit-store/new']);
  }

  disableStore(id: string) {
    this.establishmentService.changeStatusEstablishment(id, false).subscribe(
      (suc) => {
        this.isSpinningStores = true;
        this.ownerService.getStores();
        let message = {
          en: 'Store disabled',
          es: 'Establecimiento deshabilitado',
          fr: 'Établissement désactivé'
        };
        this.blService.createMessage('success', message);
      },
      (err) => {
        let message = {
          en: 'There was an error. Store not disabled',
          es: 'Hubo un problema. Establecimiento no deshabilitado',
          fr: 'Il y avait une erreur. Établissement non désactivé'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  enableStore(id: string) {
    this.establishmentService.changeStatusEstablishment(id, true).subscribe(
      (suc) => {
        this.isSpinningStores = true;
        this.ownerService.getStores();
        let message = {
          en: 'Store enabled',
          es: 'Establecimiento habilitado',
          fr: 'Établissement activé'
        };
        this.blService.createMessage('success', message);
      },
      (err) => {
        let message = {
          en: 'There was an error. Store not enabled',
          es: 'Hubo un problema. Establecimiento no habilitado',
          fr: 'Il y avait une erreur. Établissement non activé'
        };
        this.blService.createMessage('error', message);
      }
    );
  }
  
  deleteStore(id: string) {
    // TODO RENAME Store to Establishment to have consistence with backend model...
    this.establishmentService.deleteEstablishment(id).subscribe(
      (suc) => {
        this.isSpinningStores = true;
        this.ownerService.getStores();
        let message = {
          en: 'Store deleted',
          es: 'Establecimiento eliminado',
          fr: 'Établissement supprimé'
        };
        this.blService.createMessage('success', message);
      },
      (err) => {
        let message = {
          en: 'There was an error. Store not deleted',
          es: 'Hubo un problema. Establecimiento no eliminado',
          fr: 'Il y avait une erreur. Établissement non supprimé'
        };
        this.blService.createMessage('error', message);
      }
    );
  }

  resetPaginator() {
    this.manualPage = 1;
    this.offset = 0;
    if (this.paginator) {
        this.paginator.pageIndex = 0;
    }
    this.goToPage();
  }

  getDays(establishment) {
    let days = establishment.opening_hours?.day;
    if(!days) return;
    if(days.length === 7) return this.AllDaysText[this.languageService.language]
    let retVal = [];
    days.forEach((item) => {
      retVal.push(this.days[item][this.languageService.language]);
    });
    return retVal.join(',');
  }

  getHours(establishment) {
    let openHour = new Date(establishment.opening_hours.open_hour).getUTCHours();
    let closeHour = new Date(establishment.opening_hours.close_hour).getUTCHours();
    return (openHour > 12 ? openHour-12 + ' PM' : openHour === 12 ? openHour + ' PM' : openHour + ' AM') + ' - ' +  (closeHour > 12 ? closeHour-12 + ' PM' : closeHour === 12 ? closeHour + ' PM' : closeHour + ' AM');
  }

  // Change paginations values when selected by user
  onChangePage(ev) {
    this.currentPage = ev.pageIndex;
    this.offset = elementsPerPage * this.currentPage;
    this.manualPage = this.currentPage + 1;
    this.goToPage();
  }

  // Change pagination page manually
  updateManualPage(ev) {
    this.currentPage = ev - 1;
    this.offset = elementsPerPage * this.currentPage;
    this.paginator.pageIndex = ev - 1;
    this.goToPage();
  }

  goToPage() {
    this.dataSource = this.dataSet.slice(this.offset, elementsPerPage+this.offset);
  }

  ngOnDestroy() {
    this.ownerSub.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { passwordValidator } from '../app/validators/password.validator';
import { BusinessLogicService } from '../app/services/businessLogic.service';
import { finalize } from 'rxjs/operators';
import { UserAuthService } from '../app/services/user-auth.service';
import { OwnerAuthService } from '../app/services/owner-auth.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
  role:string = 'user';
  token:string = '';
  validateForm: FormGroup;
  isSpinningSave: boolean = false;


  constructor(
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private blService: BusinessLogicService,
    private userAuthService: UserAuthService,
    private ownerAuthService: OwnerAuthService,
    private router: Router
  ) { 
    this.role = this.activeRoute.snapshot.params['role'];
    this.token = this.activeRoute.snapshot.params['token'];
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      password: [null, [Validators.required, Validators.minLength(8), passwordValidator]],
      password2: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    let password = this.validateForm.controls['password'].value;
    let password2 = this.validateForm.controls['password2'].value;
    if(password !== password2) {
      let message = {
        en: 'Passwords are not equal',
        es: 'Las contraseñas no son iguales',
        fr: 'Les mots de passe ne sont pas égaux'
      };
      this.blService.createMessage('error', message);
      return;
    }
    this.isSpinningSave = true;
    let service = this.role === 'owner' ? this.ownerAuthService : this.userAuthService;
    service.changeForgottenPassword(password, this.token).pipe(
      finalize(() => {this.isSpinningSave = false;} )
    ).subscribe(
      success => {
        let route = this.role === 'owner' ? '/owner' : '/user';
        this.router.navigateByUrl(route);
        let message = {
          en: 'Your password has been changed correctly',
          es: 'Su contraseña ha sido cambiada correctamente',
          fr: 'Votre mot de passe a été changé correctement'
        };
        this.blService.createMessage('success', message);
      },
      err => {
        let message = {
          en: err.error.message.en || 'There was a problem registering. Please check your connection',
          es: err.error.message.es || 'Hubo un problema al registrarse. Por favor verifique su conexión',
          fr: err.error.message.fr || 'Un problème est survenu lors de l\'enregistrement. Veuillez vérifier votre connexion'
        };
        this.blService.createMessage('success', message);
      }
    );
  }

}
